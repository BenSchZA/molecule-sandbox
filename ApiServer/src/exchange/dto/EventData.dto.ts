import { IsNumber, IsDefined, IsString } from 'class-validator';

export class EventDataDTO {
  @IsNumber()
  public blockNumber: number;

  @IsString()
  public transactionHash: string;

  @IsDefined()
  public event: any;
}