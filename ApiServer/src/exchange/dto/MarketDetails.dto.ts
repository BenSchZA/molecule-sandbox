import { IsString, IsNumber } from 'class-validator';

export class MarketDetailsDTO {
  @IsString()
  public id: string;

  @IsString()
  public compoundName: string;

  @IsNumber()
  public price: number;

  @IsNumber()
  public totalSupply: number;

  @IsNumber()
  public marketCap: number;

  @IsNumber()
  public balance: number;

  @IsNumber()
  public amountInEscrow: number;
}