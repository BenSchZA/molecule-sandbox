import { Controller, Get, Post, UseGuards, Body, Req, HttpException, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import Express from 'express';
import { ExchangeService } from './exchange.service';
import { UsersService } from 'src/user/users.service';
import { TransactionDataPointDTO } from './dto/TransactionDataPoint.dto';
import { MarketDetailsDTO } from './dto/MarketDetails.dto';

@Controller('exchange')
export class ExchangeController {
  constructor(private readonly exchangeService: ExchangeService,
              private readonly userService: UsersService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async getAll(@Req() request: Express.Request & {user: any}): Promise<MarketDetailsDTO[] | HttpException> {
    const userWallet = await this.userService.unlockWallet(request.user);
    return await this.exchangeService.getMarketsDetails(userWallet);
  }

  @Post(':id/transactionHistory')
  @UseGuards(AuthGuard('jwt'))
  async getTransactionHistory(
      @Req() request: Express.Request & { user: any },
      @Param('id') patentId): Promise<TransactionDataPointDTO[]> {
    const userWallet = await this.userService.unlockWallet(request.user);
    return await this.exchangeService.getTransactionHistory(userWallet, patentId);
  }

  @Post(':id/marketHistory')
  @UseGuards(AuthGuard('jwt'))
  async getMarketHistoryDataPoints(
      @Req() request: Express.Request & { user: any },
      @Param('id') patentId): Promise<TransactionDataPointDTO[]> {
    const userWallet = await this.userService.unlockWallet(request.user);
    return await this.exchangeService.getMarketHistoryDataPoints(userWallet, patentId);
  }
  
  @Get(':id/buyCost/:tokenAmount')
  @UseGuards(AuthGuard('jwt'))
  async buyCost(@Req() request: Express.Request & {user: any}, 
                @Param('id') patentId,
                @Param('tokenAmount') tokenAmount): Promise<any | HttpException> {
    const userWallet = await this.userService.unlockWallet(request.user);
    const result = await this.exchangeService.buyCostCompoundTokens(userWallet, patentId, tokenAmount);
    return {buyCost: result};
  }
  
  @Get(':id/sellReward/:tokenAmount')
  @UseGuards(AuthGuard('jwt'))
  async sellReward(@Req() request: Express.Request & {user: any},
                   @Param('id') patentId,
                   @Param('tokenAmount') tokenAmount): Promise<any | HttpException> {
    const userWallet = await this.userService.unlockWallet(request.user);
    const result = await this.exchangeService.sellRewardCompoundTokens(userWallet, patentId, tokenAmount);
    return {sellReward: result};
  }

  @Post(':id/buy')
  @UseGuards(AuthGuard('jwt'))
  async buy(@Req() request: Express.Request & {user: any},
            @Param('id') patentId,
            @Body() body: { tokenAmount: number }): Promise<any | HttpException> {
    const userWallet = await this.userService.unlockWallet(request.user);
    return await this.exchangeService.buyCompoundTokens(userWallet, patentId, body.tokenAmount);
  }

   @Post(':id/sell')
  @UseGuards(AuthGuard('jwt'))
  async sell(@Req() request: Express.Request & {user: any},
             @Param('id') patentId,
             @Body() body: { tokenAmount: number }): Promise<any | HttpException> {
    const userWallet = await this.userService.unlockWallet(request.user);
    return await this.exchangeService.sellCompoundTokens(userWallet, patentId, body.tokenAmount);
  }
}
