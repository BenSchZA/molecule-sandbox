import { Module } from '@nestjs/common';
import { ExchangeController } from './exchange.controller';
import { ExchangeService } from './exchange.service';
import { MarketFactoryModule } from '../marketfactory/marketfactory.module';
import { MarketModule } from '../market/market.module';
import { NftRegistryModule } from '../nftregistry/nftregistry.module';
import { UsersModule } from 'src/user/users.module';
import { PatentModule } from 'src/patent/patent.module';

@Module({
  imports: [
    MarketFactoryModule,
    MarketModule,
    NftRegistryModule,
    UsersModule,
    PatentModule
  ],
  controllers: [ExchangeController],
  providers: [ExchangeService],
  exports: [ExchangeService],
})

export class ExchangeModule {}
