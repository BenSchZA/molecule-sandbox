import { Document, Schema, Types } from 'mongoose';
import { Schemas } from 'src/app.constants';
import { UserDocument } from '../user/user.schema';

export interface Organization {
    name: string;
    registrationNumber: string;
    owner: UserDocument ;
    members: UserDocument[];
    addMember(member: UserDocument): void;
    removeMember(member: UserDocument): void;
}

export interface OrganizationDocument extends Organization, Document { }

export const organizationSchema = new Schema({
    name: {type: String, required: true},
    registrationNumber: {type: String, required: true},
    owner: {type: Schema.Types.ObjectId, ref: Schemas.User},
    members: [{type: Schema.Types.ObjectId, ref: Schemas.User}],
}, {
    timestamps: true,
    toJSON: {
        getters: true,
        versionKey: false,
        transform: (doc, ret) => {
            ret.id = String(ret._id);
            delete ret._id;
            return ret;
        },
        virtuals: true,
    },
    toObject: {
        getters: true,
        versionKey: false,
        transform: (doc, ret) => {
            ret.id = String(ret._id);
            delete ret._id;
            return ret;
        },
    },
});

organizationSchema.method({
    addMember: function(this: OrganizationDocument, member: UserDocument) {
        this.members.push(member);
    },
    removeMember: function(this: OrganizationDocument, member: UserDocument) {
        const newMembers = this.members.filter(
            m => m._id !== member._id,
        );
        this.members = newMembers;
    },
});
