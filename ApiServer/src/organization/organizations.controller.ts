import { Body, Controller, Get, HttpException, Param, Post, Req, UnauthorizedException, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateOrganizationDTO } from './dto/create-organization.dto';
import { Organization, OrganizationDocument } from './organization.schema';
import { OrganizationsService } from './organizations.service';
import Express from 'express';

@Controller('organizations')
export class OrganizationsController {
  constructor(private readonly organizationService: OrganizationsService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  public async create(@Body() organisation: CreateOrganizationDTO,
                      @Req() request: Express.Request & {user: any}):
                      Promise<OrganizationDocument | HttpException> {
    return this.organizationService.create(organisation, request.user);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  public async getAll(): Promise<Organization[]> {
    return this.organizationService.getAll();
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/:orgId/addMember')
  public async addMember(@Param('orgId') orgId: string,
                         @Body() data: {memberId: string},
                         @Req() request: Express.Request & {user: any})
                         : Promise<OrganizationDocument | UnauthorizedException> {
    return this.organizationService.addMember(orgId, data.memberId, request.user.id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('/:orgId/removeMember')
  public async removeMember(@Param('orgId') orgId: string,
                            @Body() data: {memberId: string},
                            @Req() request: Express.Request & {user: any})
                            : Promise<OrganizationDocument | UnauthorizedException> {
    return this.organizationService.removeMember(orgId, data.memberId, request.user.id);
  }
}
