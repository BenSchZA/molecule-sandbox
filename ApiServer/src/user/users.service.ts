import { Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ethers } from 'ethers';
import { Model } from 'mongoose';
import { Modules, Schemas } from 'src/app.constants';
import { WalletsService } from 'src/wallet/wallets.service';
import { Logger } from 'winston';
import { ConfigService } from '../config/config.service';
import { PdaiService } from '../pdai/pdai.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { User } from './user.schema';
import { UserDocument } from './user.schema';

@Injectable()
export class UsersService {

  constructor(@InjectModel(Schemas.User) private readonly userRepository: Model<UserDocument>,
              @Inject(Modules.EthersProvider) private readonly ethersProvider: ethers.providers.Provider,
              @Inject(Modules.Logger) private readonly logger: Logger,
              private readonly walletService: WalletsService,
              private readonly config: ConfigService,
              private readonly pdaiService: PdaiService) { }

  public async create(createUserDto: CreateUserDTO): Promise<User> {
    const createdUser = new this.userRepository(createUserDto);
    const temp = await createdUser.save();
    const wallet = await this.walletService.createJSONWallet(temp.password);
    createdUser.wallet = wallet;
    const unlockedWallet = await this.walletService.unlockWallet(wallet, temp.password);
    try {
      const testAccountWallet = new ethers.Wallet(this.config.get('testAccount').privateKey, this.ethersProvider);
      const etherTx = await this.walletService.sendEther(testAccountWallet, unlockedWallet.address, '0.01');
      const mintTx = await this.pdaiService.mint(unlockedWallet);
    } catch (error) {
      this.logger.log('error', 'Something went wrong while creating this wallet');
      this.logger.log('error', error.message);
    }

    await createdUser.save();
    return createdUser.toObject();
  }

  public async findByEmail(email: string): Promise<UserDocument> {
    const result = await this.userRepository.findOne({ email: email }).select('+password +wallet');
    return result;
  }

  public async findById(userId: string): Promise<User | false> {
    const result = await this.userRepository.findById(userId);
    return result && result.toObject();
  }

  public async unlockWallet(user: User): Promise<ethers.Wallet> {
    const userWithPassword = await this.findByEmail(user.email);
    return await this.walletService.unlockWallet(userWithPassword.wallet, userWithPassword.password);
  }
}
