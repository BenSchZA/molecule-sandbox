import { forwardRef, Module } from '@nestjs/common';
import { MarketModule } from 'src/market/market.module';
import { MarketFactoryModule } from 'src/marketfactory/marketfactory.module';
import { NftRegistryModule } from 'src/nftregistry/nftregistry.module';
import { PatentModule } from 'src/patent/patent.module';
import { PdaiModule } from 'src/pdai/pdai.module';
import { UsersModule } from 'src/user/users.module';
import { WalletController } from './wallet.controller';
import { WalletsService } from './wallets.service';

@Module({
  imports: [forwardRef(() => UsersModule),
            forwardRef(() => PatentModule),
            PdaiModule,
            MarketFactoryModule,
            MarketModule,
            NftRegistryModule,
  ],
  controllers: [WalletController],
  providers: [WalletsService],
  exports: [WalletsService],
})

export class WalletsModule { }
