pragma solidity ^0.5.0;

import "../IEscrowFactory.sol";
import "./Escrow.sol";
import "../../MarketFactory.sol";

contract BasicEscrowFactory is IEscrowFactory{

    constructor(address _rootEscrow) 
        public
    {
        rootEscrow = _rootEscrow;
    }

    /**
      * @dev Deployes the escrow.
      * @param _nftOwner : The owner of the NFT
      * @param _marketFactory : The address of the 
      *     market factory
      */
    function deployEscrow(
        address _nftOwner,
        address _marketFactory
    ) 
        external 
        onlyRootEscrow()
        returns(address) 
    {
        address escrowAddress = address(new Escrow(_nftOwner, _marketFactory));
        return escrowAddress;
    }
}