pragma solidity ^0.5.0;

import "../IEscrow.sol";
import "../../markets/IMarket.sol";
import "../../openzeppelin-solidity/token/ERC20/IERC20.sol";

contract Escrow is IEscrow {
    address private nftOwner;

    event Deposited(address indexed payee, uint256 weiAmount);
    event Withdrawn(address indexed payee, uint256 weiAmount);

    constructor (address _nftOwner, address _marketFactory) public{
        nftOwner = _nftOwner;
        marketFactory_ = _marketFactory;
    }

    /**
      * @dev Called when the target market is deployed 
      *     to register the escrow for the NFT owners tokens.
      * @notice Ensures the Escrow has not already been registered
      *     and ensures only the Market Factory can call this function,
      */
    function updateEscrow(address _targetMarket)
        external
        notRegistered()
        onlyMarketFactory(msg.sender)
    {
        targetMarket_ = _targetMarket;
        marketRegistered_ = true;
    }

    /**
      * @dev Allows the NFT owner to withdraw their tokens 
      *     after the market has ended (the total supply 
      *     is equal to the market cap)
      */
    function withdraw() 
        external 
        returns(bool)
    {
        require(
            IMarket(targetMarket_).totalSupply() ==
            IMarket(targetMarket_).marketCap(),
            "The market has not ended"
        );
        uint256 totalBalance = IMarket(targetMarket_).balanceOf(address(this));
        IMarket(targetMarket_).transfer(nftOwner, totalBalance);
        emit Withdrawn(nftOwner, totalBalance);
        return true;
    }
}