pragma solidity ^0.5.0;

import "./BasicLinearMarket.sol";
import "../../escrowTypes/IEscrowFactory.sol";
import "../IMarketFactory.sol";

contract BasicLinearFactory is IMarketFactory {

    constructor(
        address _rootFactory, 
        address _nftRegistry, 
        address _reserveToken 
    ) 
        public 
    {
        rootFactory = _rootFactory;
        nftRegistry = _nftRegistry;
        reserveToken = _reserveToken;
    }

    function deployMarket(
        uint256 _nftTokenID,
        uint256 _initialReserve,
        uint256 _marketCap,
        address _nftOwner,
        address _escrowAddress,
        uint256 _gradient
    )
        external
        onlyRootFactory()
        returns(address)
    {
        address newMarket = address(
            new BasicLinearMarket(
                reserveToken,
                nftRegistry,
                _nftTokenID,
                _initialReserve,
                _marketCap,
                _escrowAddress,
                _gradient
            )
        );
        return newMarket;
    }
}