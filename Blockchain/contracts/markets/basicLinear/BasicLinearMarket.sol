pragma solidity ^0.5.0;

import "../../openzeppelin-solidity/math/SafeMath.sol";
import "../../openzeppelin-solidity/token/ERC20/IERC20.sol";
import "../IMarket.sol";

contract BasicLinearMarket is IMarket {
    using SafeMath for uint256;

    uint256 private poolBalance;
    address private reserveToken;
    address private nftRegistry;
    uint256 internal totalSupply_;
    uint256 internal marketCap_;
    address private escrowAddress;

    // uint8 public decimals;
    uint8 private exponent;
    address private _factory;

    uint256 private nftTokenID_;
    uint256 private initialReserve;
    uint256 private gradientDenominator;
    uint256 private decimals = 18;

    mapping (address => mapping (address => uint256)) internal allowed;
    mapping(address => uint256) internal balances;

    event Approval(
      address indexed owner,
      address indexed spender,
      uint256 value
    );

    event Transfer(address indexed from, address indexed to, uint value);
    event Transfer(address indexed from, address indexed to, uint value, bytes data);
    event Minted(uint256 amount, uint256 totalCost);
    event Burned(uint256 amount, uint256 reward);

    modifier onlyFactory() {
        require(msg.sender == _factory, "Only accesible by factory");
        _;
    }

     constructor (
        address _reserveToken,
        address _nftRegistry,
        uint256 _nftTokenID,
        uint256 _initialReserve,
        uint256 _marketCap,
        address _escrowAddress,
        uint256 _gradient
    ) public {
        reserveToken = _reserveToken;
        nftTokenID_ = _nftTokenID;
        nftRegistry = _nftRegistry;
        initialReserve = _initialReserve;
        marketCap_ = _marketCap;
        _factory = msg.sender;
        escrowAddress = _escrowAddress;
        gradientDenominator = _gradient;
    }

    /**
      * @dev Gets the balance of the specified address.
      * @param _owner The address to query the the balance of.
      * @return An uint256 representing the amount owned by the passed address.
      */
    function balanceOf(address _owner) public view returns (uint256) {
        return balances[_owner];
    }

    /**
      * @dev Total number of tokens in existence
      */
    function totalSupply() public view returns (uint256) {
        return totalSupply_;
    }

    function marketCap() public view returns(uint256) {
        return marketCap_;
    }

    function nftTokenID() external view returns (uint256) {
        return nftTokenID_;
    }

    /**
      * @dev Transfer ownership token from msg.sender to a specified address
      * @param _to The address to transfer to.
      * @param _value The amount to be transferred.
      */
    function transfer(address _to, uint256 _value) public returns (bool) {
        require(_value <= balances[msg.sender]);
        require(_to != address(0));

        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);
        emit Transfer(msg.sender, _to, _value);
        return true;
    }


    /**
     * @dev Transfer tokens from one address to another
     * @param _from address The address which you want to send tokens from
     * @param _to address The address which you want to transfer to
     * @param _value uint256 the amount of tokens to be transferred
     */
    function transferFrom(
        address _from,
        address _to,
        uint256 _value
    )
        public
        returns (bool)
    {
        require(_value <= balances[_from]);
        require(_value <= allowed[_from][msg.sender]);
        require(_to != address(0));

        balances[_from] = balances[_from].sub(_value);
        balances[_to] = balances[_to].add(_value);
        allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
        emit Transfer(_from, _to, _value);
        return true;
    }


    /// @dev        Calculate the integral from 0 to x tokens supply
    /// @param x    The number of tokens supply to integrate to
    /// @return     The total supply in tokens, not wei
    function curveIntegral(uint256 x) internal view returns (uint256) {
        /** This is the formula for the curve
            y = gradient*x + c
            y and x are the y & x axis respectivly 
            the gradient is the gradient of the curve 
            c is the offset, which is set to 0 for now 
            as we arnt making custom curves.
        */
        uint256 c = 0;

        // Calculate integral of y = gradient*x + c >> 0.5mx^2 + cx; where c = 0
         return ((x**2).div(2*gradientDenominator) + c.mul(x)).div(10**decimals);
    }

    /// @return  Price, in DAI, for mint
    function priceToMint(uint256 numTokens) external view returns(uint256) {
        return curveIntegral((totalSupply_.sub(balances[escrowAddress])).add(numTokens)).sub(poolBalance);
    }

    /// @return  Reward, in DAI, for burn
    function rewardForBurn(uint256 numTokens) external view returns(uint256) {
        return poolBalance.sub(curveIntegral(totalSupply_.sub(balances[escrowAddress]).sub(numTokens)));
    }

    /// @dev                    Burn tokens to receive ether
    /// @param numTokens        The number of tokens that you want to burn
    /// @dev rewardForTokens    Value in DAI, for tokens burned
    function burn(uint256 numTokens) external {
        require(balances[msg.sender] >= numTokens);

        uint256 rewardForTokens = this.rewardForBurn(numTokens);
        totalSupply_ = totalSupply_.sub(numTokens);
        balances[msg.sender] = balances[msg.sender].sub(numTokens);
        poolBalance = poolBalance.sub(rewardForTokens);
        require(
            IERC20(reserveToken).transfer(msg.sender, rewardForTokens), 
            "Require transferFrom to succeed"
        );

        emit Burned(numTokens, rewardForTokens);
    }

    /// @dev                    Mint new tokens with ether
    /// @param numTokens        The number of tokens you want to mint
    /// @dev priceForTokens     Value in DAI, for tokens minted
    /// Notes: We have modified the minting function to tax the purchase tokens
    /// This behaves as a sort of stake on buyers to participate even at a small scale
    function mint(uint256 numTokens) external {
        uint256 priceForTokens = this.priceToMint(numTokens);
        require(
            totalSupply_.add(numTokens) <= marketCap_,
            "The number of tokens exceeds the market cap"
        );
        require(
            IERC20(reserveToken).transferFrom(msg.sender, address(this), priceForTokens), 
            "Require transferFrom to succeed"
        );
        totalSupply_ = totalSupply_.add(numTokens);
        poolBalance = poolBalance.add(priceForTokens);
        balances[msg.sender] = balances[msg.sender].add(numTokens);

        emit Minted(numTokens, priceForTokens);
        emit Transfer(address(0), msg.sender, numTokens);
    }


    function getEscrow() external view returns(address) {
        return escrowAddress;
    }

    function onERC721Received(address _operator, address _from, uint256 _tokenId, bytes calldata _data) 
        external 
        returns(bytes4)
    {
        require(_tokenId == nftTokenID_, "Invalid token ID");
        require(escrowAddress != address(0), "Escrow address not set");
        totalSupply_ = totalSupply_.add(initialReserve);
        balances[escrowAddress] = balances[escrowAddress].add(initialReserve);

        emit Minted(initialReserve, 0);

        return bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"));
    }
}
