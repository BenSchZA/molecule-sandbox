var MarketFactory = require('../build/MarketFactory.json');
var BasicLinearFactory = require('../build/BasicLinearFactory.json');
var BasicLinearMarket = require('../build/BasicLinearMarket.json');
var PseudoDaiToken = require('../build/PseudoDaiToken.json');
var NFTRegistry = require('../build/NFTRegistry.json');
var EscrowFactory = require('../build/EscrowFactory.json');
var BasicEscrowFactory = require('../build/BasicEscrowFactory.json');

const etherlime = require('etherlime');
const ethers = require('ethers');

const nftRegistrySettings = {
    name: "The coolest tokens",
    symbol: "TCT",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
}

const moleculeData = {
    name: "caffeine",
    marketData: {
        tokenID: undefined,
        initialReserve: "1000",
        marketCap: "100000.0",
        escrowAddress: undefined
    }
}

const pseudoDaiSettings = {
    name: "PseudoDai",
    symbol: "pDAI",
    decimals: 18
}

describe('Basic Linear Factory test', () => {
    let deployer;
    let adminAccount = devnetAccounts[1];
    let userAccount = devnetAccounts[2];

    let PseudoDaiInstance,
        NFTRegistryInstance,
        MarketFactoryInstance,
        BasicLinearFactoryInstance;

    /**
      * Sets up the needed contracts for testing.
      *     PDAI
      *     NFTRegistry 
      *     MarketFactory
      *     BasicLinearFactory
      *     EscrowFactory
      *     BasicEscrowFactory
      * The basic factories are then linked up to 
      *     their root factories. 
      * An NFT is minted to the userAccount
      * An escrow is created 
      */
    beforeEach('', async () => {
        deployer = await new etherlime.EtherlimeDevnetDeployer(adminAccount.secretKey);
        PseudoDaiInstance = await deployer.deploy(
            PseudoDaiToken,
            false,
            pseudoDaiSettings.name,
            pseudoDaiSettings.symbol,
            pseudoDaiSettings.decimals
        );
        NFTRegistryInstance = await deployer.deploy(
            NFTRegistry,
            false,
            nftRegistrySettings.name,
            nftRegistrySettings.symbol
        );
        MarketFactoryInstance = await deployer.deploy(
            MarketFactory,
            false,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        BasicLinearFactoryInstance = await deployer.deploy(
            BasicLinearFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        EscrowFactoryInstance = await deployer.deploy(
            EscrowFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address
        );
        BasicEscrowFactoryInstance = await deployer.deploy(
            BasicEscrowFactory,
            false,
            EscrowFactoryInstance.contract.address
        );
        await EscrowFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicEscrowFactoryInstance.contract.address);
        await MarketFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicLinearFactoryInstance.contract.address);
        let receipt = await (await NFTRegistryInstance
            .from(userAccount.wallet)
            .publishMolecule(moleculeData.name)).wait();
        moleculeData.marketData.tokenID = receipt.events[0].args.tokenId;
        await EscrowFactoryInstance
            .from(userAccount.wallet)
            .deployEscrow(
                0,
                userAccount.wallet.address
            );
        moleculeData.marketData.escrowAddress = await EscrowFactoryInstance
            .from(userAccount.wallet)
            .getEscrows(0);
    });

    describe("Deploying a market", () => {
        /**
          * Creates a market, gets the address of the newly created
          *     market and makes an instance. 
          *     The tests then get all the details of the deployed 
          *     market and checks it against what they should be.
          */
        it("Market has correct information", async () => {
            await MarketFactoryInstance
                .from(userAccount.wallet)
                .deployMarket(
                    0,
                    moleculeData.marketData.tokenID,
                    ethers.utils.parseUnits(moleculeData.marketData.initialReserve.toString(), 18),
                    ethers.utils.parseUnits(moleculeData.marketData.marketCap.toString(), 18),
                    moleculeData.marketData.escrowAddress[0]
                );
            let marketAddresses = await MarketFactoryInstance
                .from(userAccount.wallet)
                .getMarkets(0)
            let MarketInstance = await etherlime.ContractAtDevnet(BasicLinearMarket, marketAddresses[0]);
            let totalSupply = await MarketInstance
                .from(userAccount.wallet)
                .totalSupply();
            let marketCap = await MarketInstance
                .from(userAccount.wallet)
                .marketCap();
            let tokenID = await MarketInstance
                .from(userAccount.wallet)
                .nftTokenID();
            let escrowBalance = await MarketInstance
                .from(userAccount.wallet)
                .balanceOf(moleculeData.marketData.escrowAddress[0]);
            assert.equal(
                totalSupply,
                0,
                "The total supply is not 0"
            );
            assert.equal(
                ethers.utils.formatUnits(marketCap, 18),
                moleculeData.marketData.marketCap,
                "The market cap is incorrect"
            );
            assert.equal(
                tokenID.toString(),
                moleculeData.marketData.tokenID.toString(),
                "The tokenID is incorrect"
            );
            assert.equal(
                ethers.utils.formatUnits(escrowBalance, 18),
                0.0,
                "The escrow balance is not 0"
            );
        });

        /**
          * Attempts to create a market from the BasicLinearFactory instead
          *     of the MarketFactory (as it should be). Expects a revert. 
          */
        it("Market can only be created though the root factory", async () => {
            try {
                await BasicLinearFactoryInstance
                .from(userAccount.wallet)
                .deployMarket(
                    moleculeData.marketData.tokenID,
                    ethers.utils.parseUnits(moleculeData.marketData.initialReserve.toString(), 18),
                    ethers.utils.parseUnits(moleculeData.marketData.marketCap.toString(), 18),
                    userAccount.wallet.address,
                    moleculeData.marketData.escrowAddress[0]
                )
                assert.equal(true, false, "Incorrect address can create market");
            } catch (error) {
                assert.equal(true, true, "Market can only be created by correct address");
            }
            
        });
    });
});