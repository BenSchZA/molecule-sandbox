var MarketFactory = require('../build/MarketFactory.json');
var BasicLinearFactory = require('../build/BasicLinearFactory.json');
var PseudoDaiToken = require('../build/PseudoDaiToken.json');
var NFTRegistry = require('../build/NFTRegistry.json');
var EscrowFactory = require('../build/EscrowFactory.json');
var BasicEscrowFactory = require('../build/BasicEscrowFactory.json');
var Escrow = require('../build/Escrow.json');

const etherlime = require('etherlime');
const ethers = require('ethers');

const nftRegistrySettings = {
    name: "The coolest tokens",
    symbol: "TCT",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
}

const moleculeData = {
    name: "caffeine",
    marketData: {
        tokenID: undefined,
        initialReserve: "1000",
        marketCap: "100000.0",
        escrowAddress: undefined
    }
}

const pseudoDaiSettings = {
    name: "PseudoDai",
    symbol: "pDAI",
    decimals: 18
}

describe('Escrow test', () => {
    let deployer;
    let adminAccount = devnetAccounts[1];
    let userAccount = devnetAccounts[2];

    let PseudoDaiInstance,
        NFTRegistryInstance,
        MarketFactoryInstance,
        EscrowInstance,
        BasicLinearFactoryInstance,
        BasicLinearMarketInstance;

    /**
     * Sets up the needed contracts for testing.
     *     PDAI
     *     NFTRegistry 
     *     MarketFactory
     *     EscrowFactory
     *     BasicEscrowFactory
     * The basic factory is then linked up to 
     *     the root factory. 
     * An NFT is minted to the userAccount
     */
    beforeEach('', async () => {
        deployer = new etherlime.EtherlimeDevnetDeployer(adminAccount.secretKey);
        PseudoDaiInstance = await deployer.deploy(
            PseudoDaiToken,
            false,
            pseudoDaiSettings.name,
            pseudoDaiSettings.symbol,
            pseudoDaiSettings.decimals
        );
        NFTRegistryInstance = await deployer.deploy(
            NFTRegistry,
            false,
            nftRegistrySettings.name,
            nftRegistrySettings.symbol
        );
        MarketFactoryInstance = await deployer.deploy(
            MarketFactory,
            false,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        BasicLinearFactoryInstance = await deployer.deploy(
            BasicLinearFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        EscrowFactoryInstance = await deployer.deploy(
            EscrowFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address
        );
        BasicEscrowFactoryInstance = await deployer.deploy(
            BasicEscrowFactory,
            false,
            EscrowFactoryInstance.contract.address
        );
        await EscrowFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicEscrowFactoryInstance.contract.address);
        await MarketFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicLinearFactoryInstance.contract.address);
        let receipt = await (await NFTRegistryInstance
            .from(userAccount.wallet)
            .publishMolecule(moleculeData.name)).wait();
        moleculeData.marketData.tokenID = receipt.events[0].args.tokenId;
        await EscrowFactoryInstance
            .from(userAccount.wallet)
            .deployEscrow(
                0,
                userAccount.wallet.address
            );
        await EscrowFactoryInstance
            .from(userAccount.wallet)
            .deployEscrow(
                0,
                userAccount.wallet.address
            );
        let escrowAddress = await EscrowFactoryInstance
            .from(userAccount.wallet)
            .getEscrows(0);
        EscrowInstance = await etherlime.ContractAtDevnet(Escrow, escrowAddress[0]);
        await MarketFactoryInstance
            .from(userAccount.wallet)
            .deployMarket(
                0,
                moleculeData.marketData.tokenID,
                ethers.utils.parseUnits(moleculeData.marketData.initialReserve.toString(), 18),
                ethers.utils.parseUnits(moleculeData.marketData.marketCap.toString(), 18),
                escrowAddress[0]
            );
        
    });

    describe('Deploying an escrow', () => {
        it("Testing modifiers", async () => {
            try {
                EscrowInstance
                    .from(userAccount.wallet)
                    .updateEscrow(
                        MarketFactoryInstance.contract.address
                    );
                assert.equal(true, false, "Able to call update from an invalid address");
            } catch (error) {
                assert.equal(true, true, "");
            }
        });


        it("Testing withdrawal in market", async () => {
            let marketAddress = await MarketFactoryInstance
            .from(userAccount.wallet)
            .getMarkets(0);

            // BasicLinearMarketInstance = await etherlime.ContractAtDevnet(
            //     BasicLinearMarket,
            //     marketAddress[0]
            // );


        });
    });

});