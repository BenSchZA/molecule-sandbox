var NFTRegistry = require('../build/NFTRegistry.json');
var MarketFactory = require('../build/MarketFactory.json');
var BasicLinearFactory = require('../build/BasicLinearFactory.json');
var PseudoDaiToken = require('../build/PseudoDaiToken.json');
var EscrowFactory = require('../build/EscrowFactory.json');
var BasicEscrowFactory = require('../build/BasicEscrowFactory.json');

const etherlime = require('etherlime');
const ethers = require('ethers');

const nftFactorySettings = {
    name: "The coolest tokens",
    symbol: "TCT",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
}

const moleculeData = {
    name: "caffeine",
    marketData: {
        symbol: "CVF",
        tokenID: undefined,
        initialReserve: ethers.utils.parseUnits("10000.0", 18),
        reserveToken: 0x0,
        nftTokenID: 0,
        nftRegistry: 0x0,
        creatorAddress: 0x0,
        marketCap: ethers.utils.parseUnits("1000000.0", 18)
    }
}

const pseudoDaiSettings = {
    name: "PseudoDai",
    symbol: "pDAI",
    decimals: 18
}

describe('Root Market Factory test', () => {
    let deployer;
    let adminAccount = devnetAccounts[1];
    let userAccount = devnetAccounts[2];

    let PseudoDaiInstance,
        NFTRegistryInstance,
        MarketFactoryInstance,
        basicLinearFactoryInstance,
        EscrowFactoryInstance,
        BasicEscrowFactoryInstance;

    /**
     * Sets up the needed contracts for testing.
     *     PDAI
     *     NFTRegistry 
     *     MarketFactory
     *     BasicLinearFactory
     *     EscrowFactory
     *     BasicEscrowFactory
     * The basic factories are then linked up to 
     *     their root factories. 
     * An NFT is then minted to the userAccount
     */
    beforeEach('', async () => {
        deployer = new etherlime.EtherlimeDevnetDeployer(adminAccount.secretKey);
        PseudoDaiInstance = await deployer.deploy(
            PseudoDaiToken,
            false,
            pseudoDaiSettings.name,
            pseudoDaiSettings.symbol,
            pseudoDaiSettings.decimals
        );
        moleculeData.marketData.reserveToken = await PseudoDaiInstance.contract.address;
        NFTRegistryInstance = await deployer.deploy(
            NFTRegistry,
            false,
            nftFactorySettings.name,
            nftFactorySettings.symbol
        );
        MarketFactoryInstance = await deployer.deploy(
            MarketFactory,
            false,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        basicLinearFactoryInstance = await deployer.deploy(
            BasicLinearFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address,
            PseudoDaiInstance.contract.address
        );
        EscrowFactoryInstance = await deployer.deploy(
            EscrowFactory,
            false,
            MarketFactoryInstance.contract.address,
            NFTRegistryInstance.contract.address
        );
        BasicEscrowFactoryInstance = await deployer.deploy(
            BasicEscrowFactory,
            false,
            EscrowFactoryInstance.contract.address
        );
        await EscrowFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(BasicEscrowFactoryInstance.contract.address);
        await MarketFactoryInstance
            .from(adminAccount.wallet)
            .registerNewFactory(basicLinearFactoryInstance.contract.address);
        let reciept = await (await NFTRegistryInstance
            .from(userAccount.wallet)
            .publishMolecule(moleculeData.name)
        ).wait();
        moleculeData.marketData.tokenID = await (reciept.events.filter(
            event => event.eventSignature == nftFactorySettings.eventSig.transfer))[0].args.tokenId;
        let balance = await NFTRegistryInstance
            .from(userAccount.wallet)
            .balanceOf(userAccount.wallet.address);
        assert.equal(balance.toNumber(), 1, "The user has their NFT");
    });

    describe("Set up tests", () => {
        /**
         * Checks that the BasicLinearFactory is set
         *     up correctly with the correct values
         */
        it("BasicLinearFactory is correctly set up", async () => {
            let basicLinearfactoryAddress = await MarketFactoryInstance
                .from(userAccount.wallet)
                .findFactory(0);
            assert.equal(
                basicLinearFactoryInstance.contract.address,
                basicLinearfactoryAddress,
                "Factory registration error"
            );
        });
    });

    describe("Deploying a market", () => {
        it("Deploys a market and emits correct details", async () => {
            await EscrowFactoryInstance
                .from(userAccount.wallet)
                .deployEscrow(
                    0,
                    userAccount.wallet.address
                );
            let escrowAddress = await EscrowFactoryInstance
                .from(userAccount.wallet)
                .getEscrows(0);
            let receipt = await (await MarketFactoryInstance
                .from(userAccount.wallet)
                .deployMarket(
                    0,
                    moleculeData.marketData.tokenID,
                    moleculeData.marketData.initialReserve,
                    moleculeData.marketData.marketCap,
                    escrowAddress[0]
                )).wait();
            let marketAddress = await MarketFactoryInstance
                .from(userAccount.wallet)
                .getMarkets(0)
            let logs = receipt.events[0].args;
            assert.equal(
                logs._owner,
                userAccount.wallet.address,
                "Owner address incorrect"
            );
            assert.equal(
                logs._market,
                marketAddress[0],
                "Market address is incorrect"
            );
            assert.equal(
                logs._tokenId.toString(),
                moleculeData.marketData.tokenID.toString(),
                "Token ID is incorrect"
            );
            assert.equal(
                logs._marketType.toNumber(),
                0,
                "Market type is incorrect"
            );
        });

        /**
         * Ensures that the modifiers are in working order
         */
        it("Modifier checks", async () => {
            await EscrowFactoryInstance
                .from(userAccount.wallet)
                .deployEscrow(
                    0,
                    userAccount.wallet.address
                );
            let escrowAddress = await EscrowFactoryInstance
                .from(userAccount.wallet)
                .getEscrows(0);
            //Incorrect market type
            try {
                MarketFactoryInstance
                    .from(userAccount.wallet)
                    .deployMarket(
                        1,
                        moleculeData.marketData.tokenID,
                        moleculeData.marketData.initialReserve,
                        moleculeData.marketData.marketCap,
                        escrowAddress[0]
                    )
                assert.equal(true, false, "Modifier failed to prevent incorrect market deployment");
            } catch (error) {
                assert.equal(true, true, "Modifier worked as expected");
            }
            //Invalid token ID
            try {
                MarketFactoryInstance
                    .from(userAccount.wallet)
                    .deployMarket(
                        0,
                        "0x0fffffffffff",
                        moleculeData.marketData.initialReserve,
                        moleculeData.marketData.marketCap,
                        escrowAddress[0]
                    )
                assert.equal(true, false, "Modifier failed to prevent incorrect market deployment");
            } catch (error) {
                assert.equal(true, true, "Modifier worked as expected");
            }
        });
    });
});