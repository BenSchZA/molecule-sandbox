const etherlime = require('etherlime');
const ethers = require('ethers');

var NFTRegistry = require('../build/NFTRegistry.json');

const NFTRegistrySettings = {
    name: "Molecules",
    symbol: "MOLp",
    eventSig: {
        transfer: 'Transfer(address,address,uint256)'
    }
}
const moleculeData = {
    name: "caffeine"
}

describe('NFT Registry', () => {
    let deployer;
    let NFTRegistryOwner = devnetAccounts[1];
    let NFTOwner = devnetAccounts[2];
    let receivingAccount = devnetAccounts[3];

    let NFTRegistryInstance;
    let NFTRegistryContract;
    beforeEach('', async () => {
        deployer = new etherlime.EtherlimeDevnetDeployer(NFTRegistryOwner.secretKey);
        NFTRegistryInstance = await deployer.deploy(NFTRegistry, false, NFTRegistrySettings.name, NFTRegistrySettings.symbol);
        NFTRegistryContract = NFTRegistryInstance.contract;

        NFTRegistryOwner.wallet = await NFTRegistryOwner.wallet.connect(deployer.provider);
        NFTOwner.wallet = await NFTOwner.wallet.connect(deployer.provider);
        receivingAccount.wallet = await receivingAccount.wallet.connect(deployer.provider);

    });

    describe("Deployment", () => {
        it("NFT Registry deploys correctly", async () => {
            let name = await NFTRegistryContract.name();
            assert.strictEqual(
                name,
                NFTRegistrySettings.name,
                "The name of the token is correct"
            );
            let symbol = await NFTRegistryContract.symbol();
            assert.strictEqual(
                symbol,
                NFTRegistrySettings.symbol,
                "The symbol of the token is correct"
            );
            let owner = await NFTRegistryContract.getOwner();
            assert.equal(owner, NFTRegistryOwner.wallet.address, "THe owner is correct");
        });
    });

    describe("NFT management", () => {
        it("Create NFT with name and apply user as owner", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);

            await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name);
            let balance = await NFTOwnerNFTRegistryInstance.balanceOf(NFTOwner.wallet.address);
            assert.equal(balance, 1, "The user has their NFT");
        });

        it("Able to list NFT for user", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let ReceiverNFTRegistryInstance = await NFTRegistryContract.connect(receivingAccount.wallet);

            let firstReceipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let firstTokenID = (firstReceipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            let secondReceipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(`Not${moleculeData.name}`)).wait();
            let secondTokenID = (secondReceipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            let thirdReceipt = await (await ReceiverNFTRegistryInstance.publishMolecule(`StillNot${moleculeData.name}`)).wait();
            let thirdTokenId = (thirdReceipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            let publishedBlockNumber = await NFTOwnerNFTRegistryInstance.publishedBlockNumber();

            let TransferFilter = NFTOwnerNFTRegistryInstance.filters[NFTRegistrySettings.eventSig.transfer](null, NFTOwner.wallet.address);
            TransferFilter.fromBlock = publishedBlockNumber.toNumber();
            TransferFilter.toBlock = "latest";

            let rawLogs = await NFTOwner.wallet.provider.getLogs(TransferFilter);
            let allTokenIds = rawLogs.map(log => NFTOwnerNFTRegistryInstance.interface.parseLog(log))
                .map(parsedLog => parsedLog.values.tokenId.toString());
            assert.equal(allTokenIds.length, 2, "Incorrect number of tokens returned")

            assert.equal(firstTokenID, allTokenIds[0], "Token ID mismatch");
            assert.equal(secondTokenID, allTokenIds[1], "Token ID mismatch");

            // Checking after transfering 
            await ReceiverNFTRegistryInstance.transferFrom(
                receivingAccount.wallet.address,
                NFTOwner.wallet.address,
                thirdTokenId
            );

            rawLogs = await NFTOwner.wallet.provider.getLogs(TransferFilter);
            allTokenIds = rawLogs.map(log => NFTOwnerNFTRegistryInstance.interface.parseLog(log))
                .map(parsedLog => parsedLog.values.tokenId.toString());
            assert.equal(allTokenIds.length, 3, "Incorrect number of tokens returned")

            assert.equal(firstTokenID, allTokenIds[0], "Token ID mismatch");
            assert.equal(secondTokenID, allTokenIds[1], "Token ID mismatch");
            assert.equal(thirdTokenId, allTokenIds[2], "Token ID mismatch");

        });

        it("Able to list all NFTs (In Market and Not in Market)", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);

            let firstReceipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let firstTokenID = (firstReceipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            let secondReceipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(`Not${moleculeData.name}`)).wait();
            let secondTokenID = (secondReceipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            let thirdReceipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(`StillNot${moleculeData.name}`)).wait();
            let thirdTokenId = (thirdReceipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            let publishedBlockNumber = await NFTOwnerNFTRegistryInstance.publishedBlockNumber();

            let TransferFilter = NFTOwnerNFTRegistryInstance.filters[NFTRegistrySettings.eventSig.transfer](null, null);
            TransferFilter.fromBlock = publishedBlockNumber.toNumber();
            TransferFilter.toBlock = "latest";

            let rawLogs = await NFTOwner.wallet.provider.getLogs(TransferFilter);
            let allTokenIds = rawLogs.map(log => NFTOwnerNFTRegistryInstance.interface.parseLog(log))
                .map(parsedLog => parsedLog.values.tokenId.toString());

            assert.equal(firstTokenID, allTokenIds[0], "Token ID mismatch");
            assert.equal(secondTokenID, allTokenIds[1], "Token ID mismatch");
            assert.equal(thirdTokenId, allTokenIds[2], "Token ID mismatch");
        });

        it("Able to transfer NFT", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            // TODO try parseLog here
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;
            await NFTOwnerNFTRegistryInstance.transferFrom(
                NFTOwner.wallet.address,
                receivingAccount.wallet.address,
                testTokenId
            );
            let owner = await NFTOwnerNFTRegistryInstance.ownerOf(testTokenId);
            assert.equal(owner, receivingAccount.wallet.address, "The new owner is correct");
        });

        it("An address that doesn't own the NFT cannot do anything with it", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;


            let receivingAccountNFTRegistryInstance = await NFTRegistryContract.connect(receivingAccount.wallet);

            assert.revert(receivingAccountNFTRegistryInstance.transferFrom(
                NFTOwner.wallet.address,
                receivingAccount.wallet.address,
                testTokenId
            ))
        });
    });

    describe("Blacklist functions", () => {
        it("Able to blacklist NFT", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            await NFTOwnerNFTRegistryInstance.blacklist(testTokenId);
            let blackListed = await NFTOwnerNFTRegistryInstance.isBlacklisted(testTokenId);
            assert.equal(blackListed, true, "The NFT was not blacklisted");
        });

        it("Unable to transferFrom black listed NFT", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            await NFTOwnerNFTRegistryInstance.blacklist(testTokenId);

            assert.revert(NFTOwnerNFTRegistryInstance.transferFrom(
                NFTOwner.wallet.address,
                receivingAccount.wallet.address,
                testTokenId
            ))
        });

        it("Unable to approve for black listed NFT", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            await NFTOwnerNFTRegistryInstance.blacklist(testTokenId);

            assert.revert(NFTOwnerNFTRegistryInstance.approve(
                receivingAccount.wallet.address,
                testTokenId
            ))
        });

        it("Unable to get approved for black listed NFT", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            await NFTOwnerNFTRegistryInstance.blacklist(testTokenId);

            assert.revert(NFTOwnerNFTRegistryInstance.getApproved(testTokenId));
        });

        it("Is black listed", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            await NFTOwnerNFTRegistryInstance.blacklist(testTokenId);

            let blackListed = await NFTOwnerNFTRegistryInstance.isBlacklisted(testTokenId);
            assert.equal(blackListed, true, "The NFT is blacklisted");
        });
    });

    describe("Meta data", () => {
        it("Publishing blocknumber retained", async () => {
            // TODO: compare blocknumber from deploy transaction to listed number in contract
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);

            let blockNumber = await NFTOwnerNFTRegistryInstance.publishedBlockNumber();
            assert.equal(parseInt(blockNumber.toString()) > 0, true);
        });

        it("Balance of", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();

            let balance = await NFTOwnerNFTRegistryInstance.balanceOf(NFTOwner.wallet.address);
            assert.equal(balance, 1, "The user has their NFT");
        });

        it("Owner of", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            let owner = await NFTRegistryInstance.contract.ownerOf(testTokenId);
            assert.equal(NFTOwner.wallet.address, owner, "ID not matching minter")
        });

        it("Approved", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            await NFTOwnerNFTRegistryInstance.approve(
                receivingAccount.wallet.address,
                testTokenId
            );

            let approved = await NFTOwnerNFTRegistryInstance.getApproved(testTokenId);
            assert.equal(receivingAccount.wallet.address, approved, "The address is incorrect");
        });

        it("Is approved for all", async () => {
            let NFTOwnerNFTRegistryInstance = await NFTRegistryContract.connect(NFTOwner.wallet);
            let receipt = await (await NFTOwnerNFTRegistryInstance.publishMolecule(moleculeData.name)).wait();
            let testTokenId = (receipt.events.filter(event => event.eventSignature == NFTRegistrySettings.eventSig.transfer))[0].args.tokenId;

            await NFTOwnerNFTRegistryInstance.setApprovalForAll(
                receivingAccount.wallet.address,
                true
            )

            let approval = await NFTOwnerNFTRegistryInstance.isApprovedForAll(
                NFTOwner.wallet.address,
                receivingAccount.wallet.address
            );

            assert.equal(approval, true, "Is approved returned false");
        });
    });
});