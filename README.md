# Molecule WebApp
[![pipeline status](https://gitlab.com/linumlabs/molecule-sandbox/badges/staging/pipeline.svg)](https://gitlab.com/linumlabs/molecule-sandbox/commits/staging)

This is the proof of concept for the Molecule Protocol webapp.

Molecule is a software platform to accelerate innovation in the pharmaceutical industry. It connects scientists, patients and industry to advance drug development in a collaborative open market.
The next generation of Drug discovery.

**MVP Features:**
* User Login
* Wallet Creation
* PsuedoDai Minting
* Patent Create
* Patent to NFT
* NFT to Market
* Initial Mint to Escrow
* Buy & Sell against Market

![System Diagram](https://gitlab.com/linumlabs/molecule-sandbox/raw/master/.media/system-diagram.png)

## Table Of Contents
- [Structure](#structure)
- [System Requirements](#system-requirements)
- [Getting Started](#getting-started)
- [Contract Deployments](#contract-deployments)
- [Future Features](#future-features)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)

## Structure

The project is built using our [full-stack boilerplate](https://gitlab.com/linumlabs/ethers-react-redux-typescript-mongo-boilerplate).
This boilerplate provides a framework with the following features:

* Database: MongoDb
* API Server: NestJS
* Web App: React + Redux
 
## System Requirements

* NodeJS >= v10
* Yarn >= 1.0
* MongoDB instance

## Getting Started

The stack is configured as a monorepo. After configuring the various components & environment variables, the project can be spun up from the root.

1.  Clone the repo
2.  Run `yarn` in the project root to install all dependancies

### Starting Application
After configuring the `ApiServer`, `WebApp`, and `Blockchain` following the steps below, run `yarn start:dev` from the root to spin up all the necessary components.

## Configuring `ApiServer`
1. Go to project root
2. Run `cd ApiServer`
3. Run `cp .env.example .env` - this is where you will configure all environment variables
4. Input your MongoDb server details in the `MONGO-HOST=` field (this will be
    localhost if you are running mongo locally or in a docker container with 
    host networking)

## Configuring `WebApp`
1. Go to project root
2. Run `cd WebApp`
3. Make a copy of the `.env.example` file named `.env`
4. Ensure the ApiServer details in the `API_HOST=` field are correct

## Configuring `Blockchain`
### Dependencies 

*  solc@0.5.0
*  etherlime@0.9.18

### Running tests

* First install the required packages with `yarn install`
* (Assuming you have docker installed) Run `yarn start:devnets` which will start the docker container 
* To run the tests, execute `yarn test`

### Etherlime Devnet Deployer

A number of changes have been made to Etherlime, and submitted in a PR, in order to interface with Ganache alternatives. We are using these changes to run our tests as can be seen in the `package.json`: `"etherlime": "BenSchZA/etherlime#temp-feature",`.

A new Etherlime deployer class has been created, with an alternative set of test accounts pre-configured:

* Ganache accounts global variable: `accounts`
* Devnet accounts global variable: `devnetAccounts`

* Ganache deployer: `deployer = await new etherlime.EtherlimeGanacheDeployer(NodeSigner.secretKey);``
* Devnet deployer: `deployer = new etherlime.EtherlimeDevnetDeployer(NodeSigner.secretKey);`

* Ganache `ContractAt`: `basicLinearMarketInstance = await etherlime.ContractAt(BasicLinearMarket, marketAddress[0]);`
* Devnet `ContractAtDevnet`: `basicLinearMarketInstance = await etherlime.ContractAtDevnet(BasicLinearMarket, marketAddress[0]);`

### Deploying contracts to network (local/Rinkeby)

1. Configure environment variables in `.env` file (never commit to repo or expose secrets!) using `.env.example` as an example
2. If deploying to Rinkeby, comment out `RINKEBY_PRIVATE_KEY` variable, and vice-versa
3. Ensure `truffle-config.js` has `development` network details configured, Rinkeby uses Infura RPC by default
4. Run `yarn deploy:local` or `yarn deploy:rinkeby` to deploy contracts

## Contract Deployments

### Nightly

The contracts as of 09/05/2019 are deployed at the following addresses:

* PseduoDai:          0xa1367b49773D95A0f97f16E7F778f2C4B97f43fE
* NFTRegistry:        0xF6875EBEE69f9854b42a6211EeBFe352fEe5EEA6
* MarketFactory:      0xA9cEC22C6943aBe740d42d6dE3682c170e81CC0d
* BasicLinearFactory: 0xe22Dec2d2c632744393837C04b512d8C782e00FB
* EscrowFactory:      0xA1b3ca26244d1d65260AFD488e52A982fC115290
* BasicEscrowFactory: 0xa7e94F24dbBee6ebbc04AD0d85802Dbc373AB9e8

### Staging

The contracts as of 09/05/2019 are deployed at the following addresses:

* PseduoDai:          0xAe86915A8AB8591f149633439D10E159e02E5dF5
* NFTRegistry:        0xe7bD48F24c07331648D6eA94A87407E8b4258Fe1
* MarketFactory:      0x7F2c00fF4ee848Bf1960da1F83a6e6EA7358657C
* BasicLinearFactory: 0x840CE124A8B9a7C153F7DbcED2e5fAA9a66843f1
* EscrowFactory:      0xA5400e3Cb31cf3135Fd9d0410EAC15F1a11fd72e
* BasicEscrowFactory: 0xd194c4A051a05bE48285F04a26a472c12c5A52Dd

## Future Features
**TODO**

* Creating interface to generate Aragon DAOs for governance

## Authors
* [Michael Yankelev / panterazar](https://gitlab.com/panterazar): Initial work
* [Veronica Coutts / VeronicaLC](https://gitlab.com/VeronicaLC): Initial work
* [Kent Fourie / drboolean](https://gitlab.com/drboolean): Initial work
* [Titian Steiger / titiansteiger](https://gitlab.com/titiansteiger): Initial work
* [Benjamin Scholtz / BenSchZA](https://gitlab.com/BenSchZA): Initial work

See also the list of contributors who participated in this project.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Acknowledgments
**TODO**

