/**
 *
 * CompoundWalletList
 *
 */

import {
  Avatar,
  Button,
  createStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Theme,
  WithStyles,
  withStyles,
} from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import { Link } from '@material-ui/icons';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EyeIcon from '@material-ui/icons/RemoveRedEye';
import apiUrlBuilder from 'api/apiUrlBuilder';
import React from 'react';

const styles = ({ spacing, breakpoints, typography, palette }: Theme) => createStyles({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: typography.pxToRem(15),
    fontWeight: typography.fontWeightRegular,
    flexBasis: '75%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: typography.pxToRem(15),
    color: palette.secondary.light,
  },
  container: {
    display: 'block',
    marginTop: 50,
  },
  panelDetails: {
    padding: 0,
  },
  avatar: {
    margin: 10,
  },
});

interface OwnProps extends WithStyles<typeof styles> {
  compounds: any[];
  nfts: any[];
}

const CompoundWalletList: React.SFC<OwnProps> = (props: OwnProps) => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>Compounds</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.panelDetails}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Name</TableCell>
                <TableCell>Token ID</TableCell>
                <TableCell>Balance</TableCell>
                <TableCell>In Escrow</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {props.compounds &&
                props.compounds.map((row, index) => (
                  <TableRow key={row.id}>
                    <TableCell>
                      <Avatar src={apiUrlBuilder.attachmentStream(row.markushStructure)} className={classes.avatar} />
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {row.compoundName}
                    </TableCell>
                    <TableCell>
                      {row.tokenId}
                    </TableCell>
                    <TableCell>
                      {row.balance}
                    </TableCell>
                    <TableCell>
                      {row.amountInEscrow}
                    </TableCell>
                    <TableCell>
                      <Link style={{ textDecoration: 'none', color: 'black' }}>
                        <EyeIcon />
                      </Link>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>NFTs</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.panelDetails}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Name</TableCell>
                <TableCell>Token ID</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.nfts &&
                props.nfts.map((row, index) => (
                  <TableRow key={row.id}>
                    <TableCell>
                      <Avatar src={apiUrlBuilder.attachmentStream(row.markushStructure)} />
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell>
                      {row.tokenId}
                    </TableCell>
                    <TableCell>
                      {row.moleculeName}
                    </TableCell>
                    <TableCell>
                      <Link style={{ textDecoration: 'none', color: 'black' }}>
                        <EyeIcon />
                      </Link>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
};

export default withStyles(styles)(CompoundWalletList);
