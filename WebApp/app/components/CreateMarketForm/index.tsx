/**
 *
 * PublishPatentForm
 *
 */

import React from 'react';
import { Theme, createStyles, withStyles, WithStyles, Button, FormControl, Typography, LinearProgress, MenuItem } from '@material-ui/core';
import { Form, Field } from 'formik';
import { Save, ArrowBack } from '@material-ui/icons';
import { TextField } from 'formik-material-ui';
import { Select } from '../FormikMuiSelectControl'

const styles = ({ spacing, breakpoints }: Theme) => createStyles({
  layout: {
    width: 'auto',
    display: 'grid',
  },
  form: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignContent: 'center',
    marginTop: spacing.unit * 2,
    marginBottom: spacing.unit * 2,
    marginLeft: spacing.unit * 2,
    marginRight: spacing.unit * 2,
    padding: spacing.unit * 2,
    [breakpoints.down('md')]: {
      flexDirection: 'column'
    }
  },
  iconMargin: {
    marginRight: `5px`
  },
  controls: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonRight: {
    marginTop: spacing.unit * 2,
    alignItems: 'right',
  },
  buttonLeft: {
    marginTop: spacing.unit * 2,
    alignItems: 'left',
  },
  textStyling: {
    marginTop: spacing.unit * 2,
  },
});

const curveTypes = [{
  id: 0,
  name: 'Linear Curve',
}];

interface OwnProps extends WithStyles<typeof styles> {
  submitForm(): void;
  closeDialog(): void;
  isSubmitting: boolean;
}

const CreateMarketForm: React.SFC<OwnProps> = (props: OwnProps) => {
  const { classes, submitForm, closeDialog, isSubmitting } = props;

  return (
    <Form className={classes.form}>
      <Typography component="h1" variant="h4">
        Creating your market
      </Typography>
      <FormControl margin="normal" required fullWidth>
        <Field name="patentId" label="id" component={TextField} style={{ display: 'none' }} />
        <Field
          className={classes.textStyling}
          name="curveType"
          inputProps={{
            id: 'curveTypes',
          }}
          label="Curve Type"
          component={Select}
          fullWidth
          value="" >
          {curveTypes.map(pt => <MenuItem key={pt.id} value={pt.id}>{pt.name}</MenuItem>)}
        </Field>
        <Field
          className={classes.textStyling}
          name="gradient"
          type="number"
          label="Gradient"
          component={TextField}
          InputProps={{
            inputProps: {
              min: 1,
              step: 1,
            }
          }} />
        <Field
          className={classes.textStyling}
          name="marketCap"
          type="number"
          label="Total Token Supply"
          component={TextField}
          InputProps={{
            inputProps: {
              min: 1,
              step: 1,
            }
          }} />
        <Field
          className={classes.textStyling}
          name="initialReserve"
          type="number"
          label="Retained Shared (in %)"
          component={TextField} 
          InputProps={{
            inputProps: {
              min: 0,
              max: 100,
              step: 1,
            }
          }}/>
        {isSubmitting && <LinearProgress />}
      </FormControl>
      <div className={classes.controls}>
        <Button
          variant="contained"
          color="secondary"
          className={classes.buttonLeft}
          disabled={isSubmitting}
          onClick={closeDialog}>
          <ArrowBack className={classes.iconMargin} />
          Cancel
        </Button>
        <Button
          variant="contained"
          color="primary"
          className={classes.buttonRight}
          disabled={isSubmitting}
          onClick={submitForm}>
          <Save className={classes.iconMargin} />
          Submit
      </Button>
      </div>
    </Form>
  );
};

export default withStyles(styles, { withTheme: true })(CreateMarketForm);
