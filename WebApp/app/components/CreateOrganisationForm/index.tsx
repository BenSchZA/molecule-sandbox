/**
 *
 * CreateOrganisationForm
 *
 */

import {
  Button,
  createStyles,
  FormControl,
  LinearProgress,
  Paper,
  Theme,
  Typography,
  withStyles,
  WithStyles,
} from '@material-ui/core';
import { Field, Form } from 'formik';
import { TextField } from 'formik-material-ui';
import React, {Fragment} from 'react';

const styles = ({ spacing, breakpoints }: Theme) => createStyles({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: spacing.unit * 3,
    marginRight: spacing.unit * 3,
    [breakpoints.up(400 + spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${spacing.unit * 2}px ${spacing.unit * 3}px ${spacing.unit * 3}px`,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: spacing.unit,
  },
  submit: {
    marginTop: spacing.unit * 3,
  },
});

interface OwnProps extends WithStyles<typeof styles> {
  submitForm: (data) => void;
  error: string;
  isSubmitting: boolean;
}

const CreateOrganisationForm: React.SFC<OwnProps> = (props: OwnProps) => {
  const { classes, isSubmitting, submitForm, error } = props;
  return (
    <Fragment>
      <main className={classes.layout}>
      <Paper className={classes.paper}>
            <Typography variant="h4">Create Organisation</Typography>
            <Form className={classes.form}>
              <FormControl margin="normal" required fullWidth>
                <Field
                  name="name"
                  type="text"
                  label="Name"
                  component={TextField} />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <Field
                  name="registrationNumber"
                  type="text"
                  label="Registration Number"
                  component={TextField} />
              </FormControl>
              {error && <Typography variant="body1">{error}</Typography>}
              {isSubmitting && <LinearProgress />}
              <br />
              <Button
                variant="contained"
                className={classes.submit}
                color="primary"
                disabled={isSubmitting}
                fullWidth
                onClick={submitForm}>
                Submit
              </Button>
            </Form>
          </Paper>
      </main>
    </Fragment>
  );
};

export default withStyles(styles, { withTheme: true})(CreateOrganisationForm);
