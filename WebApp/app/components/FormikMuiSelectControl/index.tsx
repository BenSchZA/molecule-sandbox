
import React, { Fragment } from 'react';
import MuiSelect, { SelectProps as MuiSelectProps } from '@material-ui/core/Select';
import FormHelperText, { FormHelperTextProps } from '@material-ui/core/FormHelperText'
import { FieldProps, getIn } from 'formik';

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export interface SelectProps
  extends FieldProps,
  Omit<MuiSelectProps, 'value'> { }

export const fieldToSelect = ({
  field,
  form: { isSubmitting },
  disabled = false,
  ...props
}: SelectProps): MuiSelectProps => {
  return {
    disabled: isSubmitting || disabled,
    ...props,
    ...field,
  };
};

export const fieldFormHelperText = ({
  field,
  form,
}: SelectProps): FormHelperTextProps => {
  const { name } = field;
  const { touched, errors } = form;

  const fieldError = getIn(errors, name);
  const showError = getIn(touched, name) && !!fieldError;

  return {
    ...field,
    error: showError,
    children: showError && fieldError,
  }
}

export const Select: React.ComponentType<SelectProps> = (props: SelectProps) => { 
  const { name } = props.field;
  const { touched, errors } = props.form;
  const fieldError = getIn(errors, name);
  const showError = getIn(touched, name) && !!fieldError;
  return (
  <Fragment>
    <MuiSelect {...fieldToSelect(props)} />
    {showError && <FormHelperText {...fieldFormHelperText(props)} />}
  </Fragment>
)};

Select.displayName = 'FormikMaterialUISelect';