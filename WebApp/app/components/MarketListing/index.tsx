/**
 *
 * MarketListing
 *
 */

import React, { Fragment } from 'react';
import {
  Theme,
  createStyles,
  withStyles,
  WithStyles,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Typography,
  TableFooter,
  TablePagination
} from '@material-ui/core';
import { blueGrey } from '@material-ui/core/colors';
import { forwardTo } from 'utils/history';
import { MarketDetails } from 'domain/exchange/types';
import TablePaginationActionsWrapped from 'components/TablePagination/index'

const styles = ({ spacing, breakpoints, palette }: Theme) =>
  createStyles({
    layout: {
      width: 'auto',
      display: 'block', // Fix IE 11 issue.
      marginLeft: spacing.unit * 3,
      marginRight: spacing.unit * 3,
      [breakpoints.up(400 + spacing.unit * 3 * 2)]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      overflowX: 'auto',
      width: '100%',
    },
    tableTitle: {
      padding: spacing.unit * 2,
    },
    tableHeader: {
      backgroundColor: blueGrey[100],
    },
    tableRow: {
      cursor: 'pointer',
      '&:nth-of-type(even)': {
        backgroundColor: palette.background.default,
      },
    },
    root: {
      "&$selected": {
        backgroundColor: blueGrey[300],
      },
      "&:hover": {
        backgroundColor: blueGrey[100] + ' !important',
      }
    },
    selected: {},
    tableHeadCell: {
      fontSize: '1rem',
    },
  });

interface OwnProps extends WithStyles<typeof styles> {
  exchange: Array<MarketDetails>;
  currentMarketId: string | undefined;
}

class MarketListing extends React.Component<OwnProps> {
  state = {
    page: 0,
    rowsPerPage: 5,
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  
  handleChangeRowsPerPage = (event) => {
    this.setState({ page: 0, rowsPerPage: event.target.value });
  }

  render() {
    const { classes, exchange, currentMarketId } = this.props;
    const { rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, exchange.length - page * rowsPerPage);
    const rows = exchange && exchange.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

    return (
      <Fragment>
        <section className={classes.layout}>
          <Paper className={classes.container}>
            <Typography className={classes.tableTitle} variant="h5">
              Market Listing
            </Typography>
            <Table>
              <TableHead className={classes.tableHeader}>
                <TableRow>
                  <TableCell className={classes.tableHeadCell}>Name</TableCell>
                  <TableCell className={classes.tableHeadCell}>Price</TableCell>
                  <TableCell className={classes.tableHeadCell}>Supply</TableCell>
                  <TableCell className={classes.tableHeadCell}>Balance</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows &&
                  rows.map(row => (
                    <TableRow 
                      className={classes.tableRow}
                      classes={{
                        root: classes.root,
                        selected: classes.selected,
                      }}
                      key={row.id} 
                      hover
                      selected={row.id === currentMarketId} 
                      onClick={() => forwardTo(`/exchange/${row.id}`)} >
                      <TableCell component="th" scope="row">
                        {row.compoundName}
                      </TableCell>
                      <TableCell>
                        {row.price}
                      </TableCell>
                      <TableCell>
                        {row.totalSupply}
                      </TableCell>
                      <TableCell>
                        {row.balance}
                      </TableCell>
                    </TableRow>
                  ))}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 48 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
              </TableBody>
              {rows.length > 0 && <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    colSpan={6}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      native: true,
                    }}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActionsWrapped}
                  />
                </TableRow>
              </TableFooter>}
            </Table>
          </Paper>
        </section>
      </Fragment>
    );
  }
}

export default withStyles(styles)(MarketListing);
