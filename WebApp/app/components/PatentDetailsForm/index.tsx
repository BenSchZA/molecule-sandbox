/**
 *
 * CreatePatentForm
 *
 */
import React, { Fragment } from 'react';
import {
  Button,
  createStyles,
  Dialog,
  Grid,
  LinearProgress,
  Paper,
  Theme,
  Typography,
  WithStyles,
  withStyles,
  MenuItem,
  InputLabel,
  Stepper,
  Step,
  StepLabel,
  FormHelperText,
} from '@material-ui/core';
import { ArrowBack, Cancel, Save } from '@material-ui/icons';
import history from 'utils/history';
import CreateMarket from 'containers/CreateMarket';
import classNames from 'classnames';
import CreateNft from 'containers/CreateNft';
import { Field, Form } from 'formik';
import { TextField } from 'formik-material-ui';
import { Select } from '../FormikMuiSelectControl'

import FormikMuiTagControl from 'components/FormikMuiTagControl';
import UploadImageField from 'components/UploadImageField';
import UploadDocumentField from 'components/UploadDocumentField';

import dayjs from 'dayjs';
import FormikDropzoneControl from 'components/FormikDropzoneControl';

function isEmpty(obj) {
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

const styles = ({ spacing, breakpoints }: Theme) => {
  return createStyles({
    layout: {
      width: 'auto',
      display: 'grid',
      marginLeft: spacing.unit * 3,
      marginRight: spacing.unit * 3,
    },
    mainPaper: {
      padding: `0px ${spacing.unit * 3}px ${spacing.unit * 3}px`,
      width: 'auto',
      height: 'auto',
      marginLeft: spacing.unit * 3,
      marginRight: spacing.unit * 3,
      [breakpoints.up(400 + spacing.unit * 3 * 2)]: { // TODO: revise media target
        width: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    sectionPaper: {
      padding: spacing.unit * 2,
      marginBottom: spacing.unit * 3,
      width: '100%'
    },
    stepLabel: {
      color: 'grey',
      marginTop: spacing.unit * 1,
    },
    form: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
      width: '100%',
      marginTop: spacing.unit,
      padding: spacing.unit * 2,
      [breakpoints.down('md')]: {
        flexDirection: 'column',
      },
    },
    buttonRight: {
      marginLeft: spacing.unit * 3,
    },
    buttonLeft: {
      marginRight: spacing.unit * 3,
    },
    buttonHidden: {
      display: 'none',
    },
    iconMargin: {
      marginRight: `5px`,
    },
    controls: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  });
};

interface OwnProps extends WithStyles<typeof styles> {
  submitForm(): void,
  resetForm(): void,
  setFieldValue(field: string, value: any): void,
  errors: any,
  values: any,
  touched: any,
  isSubmitting: boolean,
  isValid: boolean,
  isNew: boolean,
  isOwner: boolean,
  canEdit: boolean,
  status: string,
}

interface LocalState {
  openNft: boolean,
  openMarket: boolean,
}

type State = LocalState;

class PatentDetailsForm extends React.Component<OwnProps, State> {
  state = {
    openNft: false,
    openMarket: false,
    numPages: 0,
    pageNumber: 1,
  };

  toggleNFTDialog = () => {
    this.setState({ openNft: !this.state.openNft });
  }

  toggleMarketDialog = () => {
    this.setState({ openMarket: !this.state.openMarket });
  }

  render() {
    const { classes, isSubmitting, submitForm, values, resetForm,
      touched, isValid, isNew, isOwner, canEdit, status, setFieldValue } = this.props;

    const steps = [{
      name: 'Record Your Patent',
      description: 'Record your patent on the Molecule Platform by providing all required details.'
    }, {
      name: 'Tokenize Your Patent',
      description: 'Create a Non-Fungible Token containing your patent information. This step is irreversible and patent details can no longer be changed after completion.'
    }, {
      name: 'Create Market',
      description: 'Define the parameters for the market used to buy and sell shares in your patent.'
    }]

    // TODO Consider getting these from the API
    const patentTypes = [{
      id: 1,
      name: "Product Claim",
    }, {
      id: 2,
      name: "Product by Process Claim",
    }, {
      id: 3,
      name: "Process Claim",
    }, {
      id: 4,
      name: "Method of Use Claim",
    }, {
      id: 5,
      name: "Formulation Claim",
    }]

    const drugTypes = [{
      id: 1,
      name: "Small molecule",
    }, {
      id: 2,
      name: "Biotech",
    }]

    const sampleTags = [{ id: 'a', text: 'aaaa' }, { id: 'b', text: 'bbbb' }, { id: 'c', text: 'cccc' }];

    const getActiveStep = () => {
      if (status === "IN_MARKET" || status === "NFT") {
        return 2;
      } else if (status === "CREATED") {
        return 1;
      } else {
        return 0;
      }
    }

    const activeStep = getActiveStep();

    const isFinalStep = () => {
      return activeStep === 2 && status === "IN_MARKET";
    }

    return (
      <Fragment>
        {isOwner &&
          <Stepper activeStep={activeStep} alternativeLabel={true}>
            {steps.map((step, index) => (
              <Step key={step.name} completed={(index < activeStep || (index === 2 && status === "IN_MARKET"))}>
                <StepLabel>{step.name}</StepLabel>
                <Typography className={classes.stepLabel} align='center'>
                  {step.description}
                </Typography>
              </Step>
            ))}
          </Stepper>
        }
        <Paper className={classes.mainPaper} square={true}>
          <Form className={classes.form}>
            <Grid container spacing={24}>
              <Grid item xs={12} sm={6}>
                <Grid container>
                  <Paper className={classes.sectionPaper}>
                    <Typography variant="h5" gutterBottom>Patent Details</Typography>
                    <Field name="id" label="id" component={TextField} style={{ display: 'none' }} />
                    <Grid container>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="patentType">Patent Type:</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="type"
                          inputProps={{
                            id: "patentType",
                          }}
                          label="Patent Type"
                          component={Select}
                          disabled={!canEdit}
                          fullWidth
                          value="" >
                          {patentTypes.map(pt => <MenuItem key={pt.id} value={pt.id}>{pt.name}</MenuItem>)}
                        </Field>
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="title">Patent Title:</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="title"
                          type="text"
                          component={TextField}
                          disabled={!canEdit}
                          fullWidth />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="uid">UID:</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="uid"
                          type="text"
                          component={TextField}
                          disabled={!canEdit}
                          fullWidth />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="abstract">Abstract:</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="abstract"
                          type="text"
                          component={TextField}
                          multiline
                          rows="5"
                          rowsMax="12"
                          disabled={!canEdit}
                          fullWidth />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="abstract">Inventors:</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="inventors"
                          inputProps={{
                            id: "inventors"
                          }}
                          disabled={!canEdit}
                          component={FormikMuiTagControl} />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="currentAssignee">Current Assignee:</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="currentAssignee"
                          type="text"
                          component={TextField}
                          disabled={!canEdit}
                          fullWidth />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="filingDate">Filing Date:</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="filingDate"
                          type="date"
                          InputProps={{
                            inputProps: {
                              max: dayjs().format('YYYY-MM-DD')
                            }
                          }}
                          placeholder="Select date"
                          component={TextField}
                          disabled={!canEdit}
                          fullWidth />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="anticipatedExpiration">Anticipated Expiration</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="anticipatedExpiration"
                          type="date"
                          component={TextField}
                          InputProps={{
                            inputProps: {
                              min: dayjs().format('YYYY-MM-DD')
                            }
                          }}
                          disabled={!canEdit}
                          InputLabelProps={{ shrink: true }}
                          fullWidth />
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid container>
                  <Paper className={classes.sectionPaper}>
                    <Typography variant="h5" gutterBottom>Drug Details</Typography>
                    <Grid container>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="drugType">Drug Type</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="drugType"
                          inputProps={{
                            id: "drugType"
                          }}
                          component={Select}
                          disabled={!canEdit}
                          fullWidth
                          value=''>
                          {drugTypes.map(dt => <MenuItem key={dt.id} value={dt.id}>{dt.name}</MenuItem>)}
                        </Field>
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="therapeuticAreas">Therapeutic Areas</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="therapeuticAreas"
                          inputProps={{
                            id: "therapeuticAreas"
                          }}
                          type="text"
                          label="Therapeutic Areas"
                          disabled={!canEdit}
                          component={FormikMuiTagControl} />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="targets">Targets</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="targets"
                          inputProps={{
                            id: "targets"
                          }}
                          type="text"
                          label="Targets"
                          disabled={!canEdit}
                          component={FormikMuiTagControl} />
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Grid container>
                  <Paper className={classes.sectionPaper}>
                    <Typography variant="h5" gutterBottom>Documents</Typography>
                    <Grid container>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="patentCertificate">Patent Certificate</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          label="Patent certificate"
                          component={UploadDocumentField}
                          inputProps={{
                            id: 'patentCertificate',
                          }}
                          name="patentCertificate"
                          disabled={!canEdit} />
                      </Grid>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="additionalDocuments">Additional Documents</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          component={FormikDropzoneControl}
                          name="additionalDocuments"
                          disabled={!canEdit} />
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid container>
                  <Paper className={classes.sectionPaper}>
                    <Typography variant="h5" gutterBottom>Relevant Visualization</Typography>
                    <Field
                      label="Relevant Visualization"
                      component={UploadImageField}
                      name="relevantVisualization"
                      disabled={!canEdit} />
                  </Paper>
                </Grid>
                <Grid container>
                  <Paper className={classes.sectionPaper}>
                    <Typography variant="h5" gutterBottom>Cooperative Patent Classification</Typography>
                    <Grid container>
                      <Grid item xs={12} md={3}>
                        <InputLabel htmlFor="cpcCodes">CPC Codes</InputLabel>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Field
                          name="cpcCodes"
                          type="text"
                          component={FormikMuiTagControl}
                          disabled={!canEdit} />
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
            </Grid>
            <div className={classes.controls}>
              <Button
                variant="contained"
                color="primary"
                fullWidth={false}
                className={classes.buttonLeft}
                onClick={history.goBack} >
                <ArrowBack className={classes.iconMargin} />
                Back
                </Button>
              <Button
                variant="contained"
                color="primary"
                disabled={isEmpty(touched) || isSubmitting}
                className={classNames(classes.buttonLeft, !canEdit && classes.buttonHidden)}
                fullWidth={false}
                onClick={resetForm} >
                <Cancel className={classes.iconMargin} />
                Cancel
                </Button>
              {isSubmitting && <LinearProgress />}
              {(isNew) ? (
                <Button
                  variant="contained"
                  color="primary"
                  fullWidth={false}
                  onClick={submitForm}
                  className={classes.buttonRight}
                  disabled={!isValid || isSubmitting}
                  hidden={!isNew}>
                  <Save className={classes.iconMargin} />
                  Save
                </Button>)
                : (isOwner) && (
                <div>
                  {activeStep === 1 ? (
                    <Fragment>
                      <Button
                        variant="contained"
                        color="primary"
                        fullWidth={false}
                        className={classes.buttonRight}
                        onClick={submitForm}
                        disabled={!isValid || isSubmitting || isEmpty(touched)}
                        hidden={!isNew}>
                        <Save className={classes.iconMargin} />
                        Update
                      </Button>
                      <Button
                        variant="contained"
                        color="primary"
                        fullWidth={false}
                        hidden={!isEmpty(touched)}
                        className={classes.buttonRight}
                        onClick={this.toggleNFTDialog}
                        disabled={!isEmpty(touched)}>
                        <Save className={classes.iconMargin} />
                        Create NFT
                      </Button>
                    </Fragment>) :
                    !isFinalStep() && (
                    <Button
                      variant="contained"
                      color="primary"
                      fullWidth={false}
                      hidden={isFinalStep()}
                      className={classes.buttonRight}
                      onClick={this.toggleMarketDialog}
                      disabled={!isEmpty(touched)}>
                      <Save className={classes.iconMargin} />
                      Create Market
                    </Button>)
                  }
                </div>
                )}
              <Dialog
                open={this.state.openNft}
                onClose={() => {if (!this.props.isSubmitting) { this.toggleNFTDialog(); }}}>
                <CreateNft patentId={values.id} closeDialog={this.toggleNFTDialog} />
              </Dialog>
              <Dialog
                open={this.state.openMarket}
                onClose={() => { if (!this.props.isSubmitting) { this.toggleMarketDialog(); }} }>
                <CreateMarket patentId={values.id} closeDialog={this.toggleMarketDialog} />
              </Dialog>
            </div>
          </Form>
        </Paper>
      </Fragment>
    );
  }
}

export default withStyles(styles, { withTheme: true })(PatentDetailsForm);
