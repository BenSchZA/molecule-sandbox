/**
 *
 * UploadButton
 *
 */

import { Button, createStyles, Dialog, FormControl, FormHelperText, Input, Theme, withStyles, WithStyles} from '@material-ui/core';
import { CloudUpload, Done } from '@material-ui/icons';
import apiUrlBuilder from 'api/apiUrlBuilder';
import { FieldProps, getIn } from 'formik';
import React, {Fragment} from 'react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import { RouteComponentProps } from 'react-router';

const styles = ({ spacing }: Theme) => createStyles({
  rightIcon: {
    marginLeft: spacing.unit,
  },
  hiddenInput: {
    display: 'none',
  },
  uploadButton: {
    marginBottom: spacing.unit,
  },
});

interface RouteParams {
  id: string; // must be type string since route params
}

interface OwnProps extends WithStyles<typeof styles>,
  RouteComponentProps<RouteParams>,
  React.Props<RouteParams>,
  FieldProps {
    disabled: boolean;
  }

interface StateProps { }

interface DispatchProps { }

interface LocalState {
  open: boolean;
  numPages: number;
  pageNumber: number;
}

type Props = StateProps & DispatchProps & OwnProps;

type State = LocalState;

class UploadDocumentField extends React.Component<Props, State> {
  public state = {
    open: false,
    numPages: 0,
    pageNumber: 1,
  };

  public onDocumentLoadSuccess = (document) => {
    const { numPages } = document;
    this.setState({
      numPages: numPages,
      pageNumber: 1,
    });
  };

  public changePage = offset => this.setState((prevState: any) => ({
    pageNumber: prevState.pageNumber + offset,
  }));

  public previousPage = () => this.changePage(-1);

  public nextPage = () => this.changePage(1);

  public toggleDialog = () => {
    this.setState({ open: !this.state.open });
  }

  public render() {
    const { classes, field, form: { touched, errors, isSubmitting, setFieldValue, setFieldTouched }, disabled } = this.props;
    const { open, numPages, pageNumber } = this.state;
    const error = getIn(touched, field.name) && getIn(errors, field.name);

    const file = typeof(field.value) === 'object' ? field.value : apiUrlBuilder.attachmentStream(field.value);

    return (
      <Fragment>
        <FormControl>
          <Input
            error={!!error}
            className={classes.hiddenInput}
            inputProps={{
              id: field.name,
              type: 'file',
              disabled: disabled || isSubmitting,
              name: field.name,
              accept: 'application/pdf',
              value: '',
              onChange: (event: any) => {
                const file = event.currentTarget.files[0];
                setFieldValue(field.name, file);
                setFieldTouched(field.name, true);
              },
              onClick: () => {
                setFieldTouched(field.name, true);
              }
            }}
          />
          <label htmlFor={field.name}>
            <Button
              variant="contained"
              component="span"
              className={classes.uploadButton}
              disabled={disabled || isSubmitting}>
              Upload
            <CloudUpload className={classes.rightIcon} />
            </Button>
            {field && field.value && !error &&
              <Button onClick={this.toggleDialog}>
                <Done />
              </Button>
            }
          </label>
          {error && <FormHelperText error>{error}</FormHelperText>}
        </FormControl>
        <Dialog
          open={open}
          onClose={this.toggleDialog}
          maxWidth={'lg'}>
          <Document
            file={file}
            onLoadSuccess={this.onDocumentLoadSuccess} >
            <Page pageNumber={pageNumber} />
          </Document>
          {(numPages > 1) &&
            <div>
              <Button
                disabled={pageNumber <= 1}
                onClick={this.previousPage}
                variant="contained">
                Previous
              </Button>
              <Button
                disabled={pageNumber >= numPages}
                onClick={this.nextPage}
                variant="contained">
                Next
              </Button>
            </div>
          }
          { (typeof(file) === 'string') &&
            <a href={file} target="_blank">
              <Button>Download</Button>
            </a>
          }
        </Dialog>
      </Fragment>
    );
  }
}

export default withStyles(styles)(UploadDocumentField);
