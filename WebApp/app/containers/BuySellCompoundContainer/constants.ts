/*
 *
 * BuySellCompoundContainer constants
 *
 */

enum ActionTypes {
  GET_BUY_COST_REQUEST = 'app/exchange/GET_BUY_COST',
  GET_BUY_COST_SUCCESS = 'app/exchange/GET_BUY_COST_SUCCESS',
  GET_BUY_COST_FAILURE = 'app/exchange/GET_BUY_COST_FAILURE',
  GET_SELL_REWARD_REQUEST = 'app/exchange/GET_SELL_REWARD',
  GET_SELL_REWARD_SUCCESS = 'app/exchange/GET_SELL_REWARD_SUCCESS',
  GET_SELL_REWARD_FAILURE = 'app/exchange/GET_SELL_REWARD_FAILURE',
  RESET = 'app/exchange/RESET',
  SET_API_SENDING_FLAG = 'app/exchange/SET_API_SENDING_FLAG',
  CANCEL_REQUEST = 'molecule/exchange/CANCEL_REQUEST_TRIGGER',
}


export default ActionTypes;
