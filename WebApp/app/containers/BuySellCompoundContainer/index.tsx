/**
 *
 * BuySellCompoundContainer
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import * as BuySellActions from './actions';
import * as ExchangeActions from '../../domain/exchange/actions';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import BuySellCompoundTokens from 'components/BuySellCompoundTokens';
import { ApplicationRootState } from 'types';
import { Formik, FormikErrors } from 'formik';
import * as Yup from 'yup';

let BuySellSchema = (state, props) => {
    return Yup.object().shape({
      tokenAmount: Yup.number()
        .min(0, 'Invalid token amount')
        .max(Number.MAX_SAFE_INTEGER, 'Invalid token amount')
        .test('sell', 'Balance too low',
          function (value) {
            if (state.activeTab !== ActiveTab.SellTab) return true
            if (!value || (value !== null && value.length === 0)) {
              return false
            }
            return value < props.compoundTokenBalance
          })
        .test('buy', 'Balance too low',
          function (value) {
            if (state.activeTab !== ActiveTab.BuyTab) return true
            if (!value || (value !== null && value.length === 0)) {
              return false
            }
            return props.costToBuy < props.daiBalance
          })
        .required('Required'),
  })};

interface OwnProps {
  patentIdFromExchange: string;
}

interface DispatchProps {
  getBuyCost(payload: { patentId: string, tokenAmount: number }): void;
  getSellReward(payload: { patentId: string, tokenAmount: number }): void;
  buy(payload: { patentId: string, tokenAmount: number }): void;
  sell(payload: { patentId: string, tokenAmount: number }): void;
  reset(): void;
  cancelRequest(): void;
}

interface StateProps {
  daiBalance: number;
  costToBuy: number;
  sellReward: number;
  currentlySending: boolean;
  currentlySendingLocal: boolean;
  compoundTokenBalance: any;
  buySellTransaction: boolean,
  buySellTransactionMessage: string | null,
}

type Props = StateProps & DispatchProps & OwnProps;

export enum ActiveTab {
  BuyTab,
  SellTab
}

class BuySellCompoundContainer extends React.Component<Props> {
  state = {
    tokenAmount: 0,
    activeTab: ActiveTab.BuyTab,
  }

  handleChangeTab = (activeTab): void => {
    this.setState({ activeTab: activeTab, tokenAmount: 0 }, this.resetForm());
    this.props.cancelRequest();
  };

  fetchData = () => {
    const {tokenAmount} = this.state;
    const {patentIdFromExchange, getBuyCost, getSellReward} = this.props;
    if(this.state.activeTab === ActiveTab.BuyTab) {
      getBuyCost({patentId: patentIdFromExchange, tokenAmount: tokenAmount});
    } else {
      getSellReward({patentId: patentIdFromExchange, tokenAmount: tokenAmount});
    }
  }

  // Formik methods
  validateForm;
  setErrors;
  resetForm;
  setFieldTouched;

  componentDidUpdate(prevProps) {
    // Wait for async action success before validation
    if(prevProps.currentlySendingLocal !== this.props.currentlySendingLocal && this.props.currentlySendingLocal == false) {
      this.validateForm().then((errors: FormikErrors<{tokenAmount: number}>) => {
        this.setErrors(errors);
        this.setFieldTouched('tokenAmount', true);
      });
    }
    if(prevProps.patentIdFromExchange !== this.props.patentIdFromExchange) {
      this.setState({ tokenAmount: 0 }, this.resetForm());
      this.props.cancelRequest();
    }
  }

  render() {
    const { 
      daiBalance,     
      patentIdFromExchange,
      buy,
      sell,
      currentlySending,
      currentlySendingLocal,
      compoundTokenBalance,
      costToBuy,
      sellReward,
      buySellTransaction,
      buySellTransactionMessage,
      reset,
    } = this.props;
    const {
      tokenAmount, 
      activeTab,
    } = this.state;

    const onChangeTokenAmount = (e) => {
      this.setState({tokenAmount: e.currentTarget.value}, this.fetchData);
    };

    return (
      <Formik
        initialValues={{
          tokenAmount: tokenAmount,
        }}
        onSubmit={(values, actions) => {
          if (activeTab === ActiveTab.BuyTab) {
            buy({patentId: patentIdFromExchange, tokenAmount: values.tokenAmount});
          } else {
            sell({patentId: patentIdFromExchange, tokenAmount: values.tokenAmount});
          }
          actions.setSubmitting(currentlySending);
        }}
        validationSchema={() => BuySellSchema(this.state, this.props)}
        validateOnChange={false}
        validateOnBlur={false}
        render={({ isValid, submitForm, resetForm, values, setFieldTouched, handleChange, handleBlur, validateForm, setErrors, initialValues }) => {
          this.validateForm = validateForm;
          this.setErrors = setErrors;
          this.resetForm = () => {resetForm(initialValues); reset()};
          this.setFieldTouched = setFieldTouched;

          return (
            <BuySellCompoundTokens
              costToBuy={costToBuy}
              sellReward={sellReward}
              handleChangeTab={this.handleChangeTab}
              daiBalance={daiBalance}
              compoundTokenBalance={compoundTokenBalance}
              activeTab={activeTab}
              submitForm={submitForm}
              onChange={onChangeTokenAmount}
              handleChange={handleChange}
              handleBlur={handleBlur}
              values={values}
              isValid={isValid}
              isSubmitting={currentlySendingLocal}
              buySellTransaction={buySellTransaction}
              buySellTransactionMessage={buySellTransactionMessage} />
            )
          }
        }
      />
    )
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    getBuyCost: (payload: { patentId: string, tokenAmount: number }) => {
      dispatch(BuySellActions.getBuyCost.request(payload));
    },
    getSellReward: (payload: { patentId: string, tokenAmount: number }) => {
      dispatch(BuySellActions.getSellReward.request(payload));
    },
    buy: (payload: { patentId: string, tokenAmount: number }) => {
      dispatch(ExchangeActions.buyCompoundTokens.request(payload));
    },
    sell: (payload: { patentId: string, tokenAmount: number }) => {
      dispatch(ExchangeActions.sellCompoundTokens.request(payload));
    },
    reset: () => {
      dispatch(BuySellActions.reset());
    },
    cancelRequest: () => {
      dispatch(BuySellActions.cancelRequest());
    },
  }
}

const mapStateToProps = (state: ApplicationRootState, props: OwnProps) => ({
  daiBalance: state.wallet.daiBalance,
  compoundTokenBalance: (state.wallet.compounds.filter(c => c.patentId === props.patentIdFromExchange)[0]) ?
                          state.wallet.compounds.filter(c => c.patentId === props.patentIdFromExchange)[0].balance : 0,
  costToBuy: state.buySellCompoundContainer.costToBuy,
  sellReward: state.buySellCompoundContainer.rewardForSell,
  currentlySending: state.app.currentlySending,
  currentlySendingLocal: state.buySellCompoundContainer.currentlySending,
  buySellTransaction: state.buySellCompoundContainer.buySellTransaction,
  buySellTransactionMessage: state.buySellCompoundContainer.buySellTransactionMessage,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer<OwnProps>({
  key: 'buySellCompoundContainer',
  reducer: reducer,
});

const withSaga = injectSaga<OwnProps>({
  key: 'buySellCompoundContainer',
  saga: saga,
});

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(BuySellCompoundContainer);
