/*
 *
 * BuySellCompoundContainer reducer
 *
 */

import * as BuySellContainerActions from './actions';
import { ContainerState, ContainerActions } from './types';
import { DomainActions as ExchangeContainerActions } from '../../domain/exchange/types'
import { getType } from 'typesafe-actions';
import * as ExchangeActions from '../../domain/exchange/actions';

export const initialState: ContainerState = {
  costToBuy: 0,
  rewardForSell: 0,
  currentlySending: false,
  buySellTransaction: false,
  buySellTransactionMessage: null,
};

function buySellCompoundContainerReducer(state: ContainerState = initialState, action: ContainerActions | ExchangeContainerActions ) {
  switch (action.type) {
    case getType(BuySellContainerActions.getBuyCost.success):
      return {
        ...state, 
        costToBuy: action.payload.buyCost
      };
    case getType(BuySellContainerActions.getSellReward.success):
      return {
        ...state, 
        rewardForSell: action.payload.sellReward
      };
    case getType(BuySellContainerActions.reset):
      return {
        ... initialState
      }
    case getType(BuySellContainerActions.setApiSendingFlag):
      return {
        ...state,
        currentlySending: action.payload
      }    
    case getType(ExchangeActions.buyCompoundTokens.request):
    case getType(ExchangeActions.sellCompoundTokens.request):
      return {
        ...state,
        buySellTransaction: true,
        buySellTransactionMessage: 'Transaction in progress',
      }
    case getType(ExchangeActions.buyCompoundTokens.success):
    case getType(ExchangeActions.sellCompoundTokens.success):
      return {
        ...state,
        buySellTransaction: false,
        buySellTransactionMessage: 'Transaction succeeded', //TODO: replace with action payload
      }
    case getType(ExchangeActions.buyCompoundTokens.failure):
    case getType(ExchangeActions.sellCompoundTokens.failure):
      return {
        ...state,
        buySellTransaction: false,
        buySellTransactionMessage: 'Transaction failed', //TODO: replace with action error message
      }
    default:
      return state;
  }
}

export default buySellCompoundContainerReducer;
