/**
 *
 * PublishPatent
 *
 */

import React, { Fragment } from 'react';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import * as Yup from 'yup';
import { createMarket } from 'domain/patent/actions';
import CreateMarketForm from 'components/CreateMarketForm';
import { ApplicationRootState } from 'types';

const MarketDataSchema = Yup.object().shape({
  initialReserve: Yup.number()
    .min(1, 'Too Small!')
    .max(100, 'Too Large!')
    .integer('Must be an integer')
    .required('Required'),
  marketCap: Yup.number()
    .min(1, 'Too Small!')
    .max(Number.MAX_SAFE_INTEGER, 'Too Large!')
    .integer('Must be an integer')
    .required('Required'),
  gradient: Yup.number()
    .min(1, 'Too Small!')
    .max(Number.MAX_SAFE_INTEGER, 'Too Large!')
    .integer('Must be an integer')
    .required('Required'),
});

interface OwnProps {
  patentId: string;
  closeDialog(): void;
}

interface DispatchProps {
  onSubmitCreateMarket(data): void;
}

interface StateProps {
  currentlySending: boolean;
 }

type Props = OwnProps & DispatchProps & StateProps;

const CreateMarket: React.SFC<Props> = (props: Props) => {
  const { patentId, onSubmitCreateMarket, closeDialog, currentlySending } = props;

  return (
    <Fragment>
        <Formik
        initialValues={{
          patentId: patentId,
          gradient: 0,
          initialReserve: 0,
          marketCap: 0,
          curveType: 0,
        }}
        isInitialValid={false}
        validationSchema={MarketDataSchema}
        onSubmit={(values, actions) => {
          onSubmitCreateMarket(values);
          actions.setSubmitting(currentlySending);
        }}
        render={({ submitForm }) =>
          (
            <CreateMarketForm
              submitForm={submitForm}
              closeDialog={closeDialog}
              isSubmitting={currentlySending}/>
          )
        }
      />
    </Fragment >
  );
};

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => {
  return {
    onSubmitCreateMarket: (data) => {
      dispatch(createMarket.request(data));
    },
  };
};

const mapStateToProps = (state: ApplicationRootState) => ({
  // error: state.loginPage.error,
  currentlySending: state.app.currentlySending,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(CreateMarket);
