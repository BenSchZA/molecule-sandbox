/*
 *
 * Organisation constants
 *
 */

enum ActionTypes {
  CREATE_ORG_REQUEST = 'molecule/Organisation/CREATE_ORG_REQUEST',
}

export default ActionTypes;
