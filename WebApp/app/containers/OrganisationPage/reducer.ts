/*
 *
 * Organisation reducer
 *
 */

import { ContainerActions, ContainerState } from './types';

export const initialState: ContainerState = {
  default: null,
};

function organisationReducer(state: ContainerState = initialState,
                             action: ContainerActions) {
  switch (action.type) {
    default:
      return state;
  }
}

export default organisationReducer;
