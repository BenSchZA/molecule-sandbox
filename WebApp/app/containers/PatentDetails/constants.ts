/*
 *
 * PatentDetails constants
 *
 */

enum ActionTypes {
  DEFAULT_ACTION = 'molecule/PatentDetails/DEFAULT_ACTION',
}

export default ActionTypes;
