/**
 *
 * PatentDetails
 *
 */

import PatentDetailsForm from 'components/PatentDetailsForm';
import { createPatent, updatePatent } from 'domain/patent/actions';
import { Formik } from 'formik';
import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { compose, Dispatch } from 'redux';
import { ApplicationRootState } from 'types';
import isMongoId from 'validator/lib/isMongoId';
import { Paper } from '@material-ui/core';
import * as Yup from 'yup';
import dayJs from 'dayjs';

interface RouteParams {
  id?: string; // must be type string since route params
}

interface OwnProps extends RouteComponentProps<RouteParams>,
  React.Props<RouteParams> { }

interface DispatchProps {
  onSubmitPatentForm(values: any): void;
}

interface StateProps {
  patent: {
    type: number,
    title: string,
    uid: string,
    abstract: string,
    inventors: [],
    currentAssignee: string,
    filingDate: Date,
    anticipatedExpiration: Date,
    drugType: 0,
    therapeuticAreas: [],
    targets: [],
    patentCertificate: string,
    additionalDocuments: [],
    relevantVisualization: string,
    cpcCodes: [],
    id: string,
    status: string,
    initialReserve: string,
    marketCap: string,
    tokenId: string,
    createdAt: Date,
    updatedAt: Date,
    createdBy: {
      id: string,
    },
  };
  userId: string;
  currentlySending: boolean,
}

type Props = StateProps & DispatchProps & OwnProps;

const MAX_FILE_SIZE = (1024 ** 2) * 10;
const SUPPORTED_IMAGE_FORMATS = [
  'image/jpg',
  'image/jpeg',
  'image/gif',
  'image/png',
];

const fileSizeValidation = (file: File | string, maxSize: number): boolean => {
  try {
    if (file === undefined) { return true; }
    if (typeof file === "string") {
      return isMongoId(file);
    } else {
      return (file.size <= maxSize);
    }
  } catch (error) {
    return true;
  }
};

const fileTypeValidation = (file: File | string, types: Array<string>): boolean => {
  try {
    if (file === undefined) { return true; }
    if (typeof file === 'string') {
      console.log('file is string');
      console.log(isMongoId(file));
      return isMongoId(file);
    } else {
      return (types.includes(file.type));
    }
  } catch (error) {
    return true;
  }
}

const PatentDetails: React.SFC<Props> = (props: Props) => {
  const { patent, onSubmitPatentForm, match: { params: { id } }, userId, currentlySending } = props;
  const isNew = (id) ? false : true;
  const PatentFormSchema = Yup.object().shape({
    type: Yup.number().min(1, 'Please select a patent type').required(),
    title: Yup.string().required(),
    uid: Yup.string().required(),
    abstract: Yup.string().required(),
    inventors: Yup.array().of(
      Yup.object({
        id: Yup.string().isRequired,
        text: Yup.string().isRequired,
      })
    ).min(1, 'Please specify at least one inventor'),
    currentAssignee: Yup.string().required(),
    filingDate: Yup.date().max(dayJs().format('YYYY-MM-DD')).required(),
    anticipatedExpiration: Yup.date().min(dayJs().format('YYYY-MM-DD')).required(),
    drugType: Yup.number().required().min(1, 'Please select a patent type'),
    therapeuticAreas: Yup.array().of(
      Yup.object({
        id: Yup.string().isRequired,
        text: Yup.string().isRequired,
      })
    ),
    targets: Yup.array().of(
      Yup.object({
        id: Yup.string().isRequired,
        text: Yup.string().isRequired,
      })
    ),
    patentCertificate: Yup.mixed().required()
      .test('fileSize', 'Maximum file size of 10MB exceeded', file => fileSizeValidation(file, MAX_FILE_SIZE))
      .test('fileType', 'Please supply a PDF file', file => fileTypeValidation(file, ['application/pdf'])),
    additionalDocuments: Yup.array().of(
      Yup.mixed()
        .test('fileSize', 'Maximum file size of 10MB exceeded', file => fileSizeValidation(file, MAX_FILE_SIZE))
        .test('fileType', 'Please supply a PDF file', file => fileTypeValidation(file, ['application/pdf'])),
    ),
    relevantVisualization: Yup.mixed().required()
      .test('fileSize', 'Maximum file size of 10MB exceeded', file => fileSizeValidation(file, MAX_FILE_SIZE))
      .test('fileType', 'Please supply an image file', file => fileTypeValidation(file, SUPPORTED_IMAGE_FORMATS)),
    cpcCodes: Yup.array().of(
      Yup.object({
        id: Yup.string().isRequired,
        text: Yup.string().isRequired,
      })
    ).min(1, 'Please specify at least one CPC Code'),
  });

  return (
    <Paper>
      <Formik
        initialValues={{
          ...patent,
          filingDate: dayJs(patent.filingDate).format('YYYY-MM-DD'),
          anticipatedExpiration: dayJs(patent.anticipatedExpiration).format('YYYY-MM-DD'),
        }}
        validationSchema={PatentFormSchema}
        onSubmit={(values, actions) => {
          onSubmitPatentForm(values);
          actions.setSubmitting(currentlySending);
        }}
        render={({ submitForm, resetForm, errors, isValid, values, touched, initialValues, setFieldValue }) =>
          <PatentDetailsForm
            errors={errors}
            isSubmitting={currentlySending}
            submitForm={submitForm}
            resetForm={() => resetForm(initialValues)}
            isValid={isValid}
            values={values}
            touched={touched}
            isNew={isNew}
            isOwner={isNew || (values.createdBy.id === userId)}
            canEdit={isNew || (values.createdBy.id === userId && values.status === 'CREATED')}
            status={values.status}
            setFieldValue={setFieldValue} />
        }
      />
    </Paper>
  );
};

const mapStateToProps = (state: ApplicationRootState, { match: { params: { id } } }: Props): StateProps => ({
  patent: id && state.patents[id] ?
    state.patents[id] : {
      type: 0,
      title: '',
      uid: '',
      abstract: '',
      inventors: [],
      currentAssignee: '',
      filingDate: '',
      anticipatedExpiration: '',
      drugType: 0,
      therapeuticAreas: [],
      targets: [],
      patentCertificate: '',
      additionalDocuments: [],
      relevantVisualization: '',
      cpcCodes: [],
      id: '',
      status: '',
      initialReserve: '',
      marketCap: '',
      tokenId: '',
      createdAt: '',
      updatedAt: '',
      createdBy: '',
    },
  userId: state.authentication.userId,
  currentlySending: state.app.currentlySending,
});

const mapDispatchToProps = (dispatch: Dispatch, props: OwnProps): DispatchProps => {
  return {
    onSubmitPatentForm: (data) => {
      (props.match.params.id) ? dispatch(updatePatent.request(data)) : dispatch(createPatent.request(data));
    },
  };
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(PatentDetails);
