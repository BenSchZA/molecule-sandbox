import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

/* --- STATE --- */
interface PatentDetailsState {
  readonly default: any;
}


/* --- ACTIONS --- */
type PatentDetailsActions = ActionType<typeof actions>;


/* --- EXPORTS --- */

type RootState = ApplicationRootState;
type ContainerState = PatentDetailsState;
type ContainerActions = PatentDetailsActions;

export { RootState, ContainerState, ContainerActions };