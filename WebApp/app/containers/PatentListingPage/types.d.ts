import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';

/* --- STATE --- */
interface PatentListingPageState {
  readonly patents: Array<any>
}


/* --- ACTIONS --- */
type PatentListingPageActions = ActionType<typeof actions>;


/* --- EXPORTS --- */

type RootState = ApplicationRootState;
type ContainerState = PatentListingPageState;
type ContainerActions = PatentListingPageActions;

export { RootState, ContainerState, ContainerActions };
