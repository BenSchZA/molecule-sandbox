/**
 *
 * WalletPage
 *
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';

import Wallet from 'components/Wallet';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import * as WalletActions from '../../domain/wallet/actions';
import reducer from './reducer';
import saga from './saga';
import { IPatent } from 'domain/patent/types';
import { CompoundTokenInformation } from './types';

interface OwnProps { }

interface StateProps {
  daiBalance: number;
  walletAddress: string;
  compounds: Array<CompoundTokenInformation>;
  nfts: Array<IPatent>;
  escrows: Array<CompoundTokenInformation>;
}

interface DispatchProps {
  getWalletData(): void;
}

type Props = StateProps & DispatchProps & OwnProps;

class WalletPage extends React.Component<Props> {
  public componentDidMount() {
    this.props.getWalletData();
  }

  public render() {
    const { daiBalance, walletAddress, nfts, escrows, compounds } = this.props;
    return (
      <Fragment>
        <Wallet
          daiBalance={daiBalance}
          compounds={compounds}
          nfts={nfts}
          walletAddress={walletAddress}
          escrows={escrows} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  ...state.wallet,
});

const mapDispatchToProps = (dispatch: Dispatch, ownProps: OwnProps): DispatchProps => ({
  getWalletData: () => {
    dispatch(WalletActions.getBalance.request());
  }
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withSaga = injectSaga<OwnProps>({ key: 'wallet', saga: saga });

export default compose(
  withSaga,
  withConnect,
)(WalletPage);
