import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import { ApplicationRootState } from 'types';
import { IPatent } from 'domain/patent/types';

export interface CompoundTokenInformation {
  patentId: string,
  tokenId: string,
  marketAddress: string,
  title: string,
  balance:number,
  escrowBalance: string,
  escrowAddress: string,
  createdBy: string,
  uid: string,
}

/* --- STATE --- */
interface WalletPageState {
  compounds: Array<CompoundTokenInformation>;
  patents: Array<IPatent>;
  nftTokens: [];
  daiBalance: number;
  walletAddress: string;
}

/* --- ACTIONS --- */
type WalletPageActions = ActionType<typeof actions>;


/* --- EXPORTS --- */

type RootState = ApplicationRootState;
type ContainerState = WalletPageState;
type ContainerActions = WalletPageActions;

export { RootState, ContainerState, ContainerActions };
