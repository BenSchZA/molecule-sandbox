import * as authenticationActions from '../actions';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { combineReducers } from 'redux';
import { fork, call } from 'redux-saga/effects';
import { createMockTask } from '@redux-saga/testing-utils';
import { throwError } from 'redux-saga-test-plan/providers';
import jwtDecode from 'jwt-decode';
import MockDate from 'mockdate';

import { login, signUp, revoke, refresh } from '../../../api/api';
import { forwardTo } from '../../../utils/history';

import rootAuthenticationSaga, {
  loginFlow,
  refreshTokenPoller,
  signupFlow,
  refreshTokenFlow,
  register,
} from '../saga';

import authenticationReducer, { initialState } from '../reducer';

describe('rootAuthenticationSaga', () => {
  const reducerUnderTest = combineReducers({
    authentication: authenticationReducer,
  });

  describe('there is no refresh token present', () => {
    test('saga starts both login and sign up watchers, and waits for the user to sign in', () => {
      return expectSaga(rootAuthenticationSaga)
        .withState({ authentication: { ...initialState } })
        .withReducer(reducerUnderTest, {
          authentication: {
            ...initialState,
          }
        })
        .fork(loginFlow)
        .fork(signupFlow)
        .not.call.fn(refreshTokenFlow)
        .not.fork(refreshTokenPoller)
        .hasFinalState({
          authentication: {
            accessToken: '',
            error: '',
            currentlySending: false,
            refreshToken: '',
            userId: '',
          }
        })
        .silentRun(50)
    });
    test('logs the user in if they provide a valid email and password', () => {
      const mockRefreshPoller = createMockTask();
      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, { authentication: { ...initialState } })
        .provide([
          [call(login, 'VALID_EMAIL', 'VALID_PASSWORD'),
          {
            success: true,
            data: {
              accessToken: 'VALID_ACCESS_TOKEN',
              refreshToken: 'VALID_REFRESH_TOKEN',
              userId: 'USER_ID',
            }
          }
          ],
          [fork(refreshTokenPoller), mockRefreshPoller],
        ])
        .fork(loginFlow)
        .fork(signupFlow)
        .fork(refreshTokenPoller)
        .put(authenticationActions.saveTokens({ accessToken: 'VALID_ACCESS_TOKEN', refreshToken: 'VALID_REFRESH_TOKEN' }))
        .call(forwardTo, '/dashboard')
        .put(authenticationActions.login.success({ userId: 'USER_ID' }))
        .hasFinalState({
          authentication: {
            accessToken: 'VALID_ACCESS_TOKEN',
            error: '',
            currentlySending: false,
            refreshToken: 'VALID_REFRESH_TOKEN',
            userId: 'USER_ID',
          }
        })
        .dispatch(authenticationActions.login.request({ email: 'VALID_EMAIL', password: 'VALID_PASSWORD' }))
        .silentRun(50)
    });
    test('handles invalid login', () => {
      const authError = new Error('Unauthorized');
      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, { authentication: { ...initialState } })
        .provide([
          [call(login, 'INVALID_EMAIL', 'VALID_PASSWORD'), throwError(authError)],
        ])
        .fork(loginFlow)
        .fork(signupFlow)
        .put(authenticationActions.login.failure('Unauthorized'))
        .hasFinalState({
          authentication: {
            accessToken: '',
            error: '',
            currentlySending: false,
            refreshToken: '',
            userId: '',
          }
        })
        .dispatch(authenticationActions.login.request({ email: 'INVALID_EMAIL', password: 'VALID_PASSWORD' }))
        .silentRun(50)
    });
  })
  describe('there is a refresh token present', () => {
    test('Refreshes the tokens if a valid refresh token is present', () => {
      const mockRefreshPoller = createMockTask();
      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, {
          authentication: {
            ...initialState,
            refreshToken: 'VALID_REFRESH_TOKEN',
            userId: 'USER_ID',
          }
        })
        .provide([
          [call(refresh, 'VALID_REFRESH_TOKEN'),
          {
            data: {
              accessToken: 'NEW_ACCESS_TOKEN',
              refreshToken: 'NEW_REFRESH_TOKEN',
            }
          }
          ],
          [fork(refreshTokenPoller), mockRefreshPoller],
        ])
        .not.fork(loginFlow)
        .not.fork(signupFlow)
        .call(refreshTokenFlow, 'VALID_REFRESH_TOKEN')
        .fork(refreshTokenPoller)
        .put(authenticationActions.saveTokens({ accessToken: 'NEW_ACCESS_TOKEN', refreshToken: 'NEW_REFRESH_TOKEN' }))
        .hasFinalState({
          authentication: {
            accessToken: 'NEW_ACCESS_TOKEN',
            error: '',
            currentlySending: false,
            refreshToken: 'NEW_REFRESH_TOKEN',
            userId: 'USER_ID',
          }
        })
        .silentRun(50)
    });
    test('handles an invalid refresh token gracefully', () => {
      const mockRefreshPoller = createMockTask();
      mockRefreshPoller.cancel = () => { };
      const refreshError = new Error('Authentication Error');

      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, {
          authentication: {
            ...initialState,
            refreshToken: 'INVALID_REFRESH_TOKEN',
            accessToken: 'EXPIRED_ACCESS_TOKEN'
          }
        })
        .provide([
          [call(refresh, 'INVALID_REFRESH_TOKEN'), throwError(refreshError)],
          [call(revoke, 'INVALID_REFRESH_TOKEN'), {}],
          [fork(refreshTokenPoller), mockRefreshPoller],
        ])
        .fork(loginFlow)
        .fork(signupFlow)
        .call(refreshTokenFlow, 'INVALID_REFRESH_TOKEN')
        .fork(refreshTokenPoller)
        .put(authenticationActions.logout())
        .put(authenticationActions.login.failure('You\'ve been logged out. Please login again.'))
        .call(revoke, 'INVALID_REFRESH_TOKEN')
        .call(forwardTo, '/')
        .hasFinalState({
          authentication: {
            accessToken: '',
            error: '',
            currentlySending: false,
            refreshToken: '',
            userId: '',
          }
        })
        .silentRun(50)
    });
    test('handles API server errors gracefully', () => {
      const mockRefreshPoller = createMockTask();
      mockRefreshPoller.cancel = () => { };
      const refreshError = new Error('Our API is broken');

      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, {
          authentication: {
            ...initialState,
            refreshToken: 'VALID_REFRESH_TOKEN',
            accessToken: 'EXPIRED_ACCESS_TOKEN'
          }
        })
        .provide([
          [call(refresh, 'VALID_REFRESH_TOKEN'), throwError(refreshError)],
          [fork(refreshTokenPoller), mockRefreshPoller],
        ])
        .call(refreshTokenFlow, 'VALID_REFRESH_TOKEN')
        .put(authenticationActions.login.failure('The server did not respond. Try again.'))
        .hasFinalState({ 
          authentication: { 
            refreshToken: 'VALID_REFRESH_TOKEN',
            error: '',
            currentlySending: false,
            accessToken: 'EXPIRED_ACCESS_TOKEN',
            userId: '' }
          })
        .silentRun(50)
    });
    test('successfully refreshes access token nearing expiry with valid refresh token', () => {
      MockDate.set(1553070800000);
      
      const aboutToExpireToken = () => ({
        userId: 'SOME_ID',
        iat: (Math.trunc(Date.now() / 1000) - (55*60)), //issued 55 minutes ago
        exp: (Math.trunc(Date.now() / 1000) + 10), //expiring in 10 seconds - 1553070810
        iss: 'FAKE_ISSUER',
        sub: '0xFAKE_SUBJECT',
      });

      const justIssuedToken = () => ({
        userId: 'SOME_ID',
        iat: (Math.trunc(Date.now() / 1000) - 1), //issued 1 sec ago
        exp: (Math.trunc(Date.now() / 1000) + (60*60)), //expiring in 1 hour
        iss: 'FAKE_ISSUER',
        sub: '0xFAKE_SUBJECT'
      });

      return expectSaga(refreshTokenPoller)
        .withReducer(reducerUnderTest, { 
          authentication: { 
            ...initialState,
            refreshToken: 'VALID_REFRESH_TOKEN',
            accessToken: 'EXPIRING_SOON_ACCESS_TOKEN',
            userId: 'USER_ID'}
          })
        .provide([
          [call(jwtDecode, 'NEW_ACCESS_TOKEN'), justIssuedToken()],
          [call(jwtDecode, 'EXPIRING_SOON_ACCESS_TOKEN'), aboutToExpireToken()],
          [call(refresh, 'VALID_REFRESH_TOKEN'), {
            data: {
              accessToken: 'NEW_ACCESS_TOKEN',
              refreshToken: 'NEW_REFRESH_TOKEN',
            }
          }]
        ])
        .call(refreshTokenFlow, 'VALID_REFRESH_TOKEN')
        .put(authenticationActions.saveTokens({accessToken: 'NEW_ACCESS_TOKEN', refreshToken: 'NEW_REFRESH_TOKEN'}))
        .delay(3240)
        .hasFinalState({
          authentication: {
            accessToken: 'NEW_ACCESS_TOKEN',
            error: '',
            currentlySending: false,
            refreshToken: 'NEW_REFRESH_TOKEN',
            userId: 'USER_ID',
          }
        })
        .silentRun(50)
    })
    test('successfully refreshes invalid access token if refresh is valid', () => {
      MockDate.set(1553070800000);
      
      const decodeError = new Error('Invalid token');

      const justIssuedToken = () => ({
        userId: 'SOME_ID',
        iat: (Math.trunc(Date.now() / 1000) - 1), //issued 1 sec ago
        exp: (Math.trunc(Date.now() / 1000) + (60*60)), //expiring in 1 hour
        iss: 'FAKE_ISSUER',
        sub: '0xFAKE_SUBJECT'
      });

      return expectSaga(refreshTokenPoller)
        .withReducer(reducerUnderTest, { 
          authentication: { 
            ...initialState,
            refreshToken: 'VALID_REFRESH_TOKEN',
            accessToken: 'INVALID_ACCESS_TOKEN',
            userId: 'USER_ID'}
          })
        .provide([
          [call(jwtDecode, 'INVALID_ACCESS_TOKEN'), throwError(decodeError)],
          [call(jwtDecode, 'NEW_ACCESS_TOKEN'), justIssuedToken()],
          [call(refresh, 'VALID_REFRESH_TOKEN'), {
            data: {
              accessToken: 'NEW_ACCESS_TOKEN',
              refreshToken: 'NEW_REFRESH_TOKEN',
            }
          }]
        ])
        .call(refreshTokenFlow, 'VALID_REFRESH_TOKEN')
        .put(authenticationActions.saveTokens({accessToken: 'NEW_ACCESS_TOKEN', refreshToken: 'NEW_REFRESH_TOKEN'}))
        .delay(3240)
        .hasFinalState({
          authentication: {
            accessToken: 'NEW_ACCESS_TOKEN',
            error: '',
            currentlySending: false,
            refreshToken: 'NEW_REFRESH_TOKEN',
            userId: 'USER_ID',
          }
        })
        .silentRun(50)
    })
  })
  describe('handles the user logging out', () => {
    test('waits for a new login after logging the user out', () => {
      const mockRefreshPoller = createMockTask();
      mockRefreshPoller.cancel = () => { };

      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, {
          authentication: {
            ...initialState,
            refreshToken: 'VALID_REFRESH_TOKEN'
          }
        })
        .provide([
          [matchers.call.fn(refreshTokenFlow), {}],
          [matchers.call.fn(revoke), {}],
          [fork(refreshTokenPoller), mockRefreshPoller],
        ])
        .call(revoke, 'VALID_REFRESH_TOKEN')
        .call(forwardTo, '/')
        .fork(loginFlow)
        .fork(signupFlow)
        .hasFinalState({
          authentication: {
            accessToken: '',
            error: '',
            currentlySending: false,
            refreshToken: '',
            userId: '',
          }
        })
        .dispatch(authenticationActions.logout())
        .silentRun(50)
    })
  })
  describe('handles the user signing up', () => {
    test('handles valid signup successfully', () => {
      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, {
          authentication: {
            ...initialState,
          }
        })
        .provide([
          [call(signUp, 'TEST@TEST.COM', 'PASSWORD', 'TEST', 'LAST'), {}],
        ])
        .fork(loginFlow)
        .fork(signupFlow)
        .call(register, 'TEST@TEST.COM', 'PASSWORD', 'TEST', 'LAST')
        .call(signUp, 'TEST@TEST.COM', 'PASSWORD', 'TEST', 'LAST')
        .hasFinalState({
          authentication: {
            accessToken: '',
            error: '',
            currentlySending: false,
            refreshToken: '',
            userId: '',
          }
        })
        .dispatch(authenticationActions.signup.request({
          email: 'TEST@TEST.COM',
          password: 'PASSWORD',
          firstName: 'TEST',
          lastName: 'LAST'
        })
        )
        .silentRun(50)
    })
    test('handles invalid signup gracefully', () => {
      const signupError = new Error('The email is already in use');
      return expectSaga(rootAuthenticationSaga)
        .withReducer(reducerUnderTest, {
          authentication: {
            ...initialState,
          }
        })
        .provide([
          [call(signUp, 'INVALID_EMAIL', 'PASSWORD', 'TEST', 'LAST'),
          throwError(signupError)],
        ])
        .fork(loginFlow)
        .fork(signupFlow)
        .call(register, 'TEST@TEST.COM', 'PASSWORD', 'TEST', 'LAST')
        .call(signUp, 'TEST@TEST.COM', 'PASSWORD', 'TEST', 'LAST')
        .put(authenticationActions.signup.failure('The email is already in use'))
        .hasFinalState({
          authentication: {
            accessToken: '',
            error: '',
            currentlySending: false,
            refreshToken: '',
            userId: '',
          }
        })
        .dispatch(authenticationActions.signup.request({
          email: 'TEST@TEST.COM',
          password: 'PASSWORD',
          firstName: 'TEST',
          lastName: 'LAST'
        })
        )
        .silentRun(50)
    })
  })
})