import { createAsyncAction } from 'typesafe-actions';
import ActionTypes from './constants';

export const getExchangeList = createAsyncAction(
  ActionTypes.GET_EXCHANGE_REQUEST,
  ActionTypes.GET_EXCHANGE_SUCCESS,
  ActionTypes.GET_EXCHANGE_FAILURE)
  <undefined, string, string>();

export const buyCompoundTokens = createAsyncAction(
  ActionTypes.BUY_REQUEST,
  ActionTypes.BUY_SUCCESS,
  ActionTypes.BUY_FAILURE)
  <{ patentId: string, tokenAmount: number }, { convertedValue: number, from: string, to: string }, string>();

export const sellCompoundTokens = createAsyncAction(
  ActionTypes.SELL_REQUEST,
  ActionTypes.SELL_SUCCESS,
  ActionTypes.SELL_FAILURE)
  <{ patentId: string, tokenAmount: number }, { convertedValue: number, from: string, to: string }, string>();