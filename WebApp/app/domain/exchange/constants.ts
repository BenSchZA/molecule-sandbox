/*
 *
 * ExchangePage constants
 *
 */

enum ActionTypes {
  GET_EXCHANGE_REQUEST = 'app/exchange/GET_EXCHANGE_REQUEST',
  GET_EXCHANGE_SUCCESS = 'app/exchange/GET_EXCHANGE_SUCCESS',
  GET_EXCHANGE_FAILURE = 'app/exchange/GET_EXCHANGE_FAILURE',
  BUY_REQUEST = 'app/exchange/BUY_REQUEST',
  BUY_SUCCESS = 'app/exchange/BUY_SUCCESS',
  BUY_FAILURE = 'app/exchange/BUY_FAILURE',
  SELL_REQUEST = 'app/exchange/SELL_REQUEST',
  SELL_SUCCESS = 'app/exchange/SELL_SUCCESS',
  SELL_FAILURE = 'app/exchange/SELL_FAILURE',
}

export default ActionTypes;
