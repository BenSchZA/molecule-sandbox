import * as ExchangeActions from './actions';
import { getType } from 'typesafe-actions';

export const initialState = {};

function exchangeReducer(state = initialState, action) {
  switch (action.type) {
    case getType(ExchangeActions.getExchangeList.success):
      return {
        ...action.payload
      }
    default:
      return state;
  }
}

export default exchangeReducer;
