import {schema} from 'normalizr';

const exchange = new schema.Entity('exchange', {}, {idAttribute: 'id'});

export default exchange;
