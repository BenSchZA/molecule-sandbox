import { createAsyncAction } from 'typesafe-actions';
import ActionTypes from './constants';
import { IPatent } from './types';

// TODO Get proper interfaces defined for each of these actions and their payloads

export const getPatents = createAsyncAction(
  ActionTypes.GET_PATENTS_REQUEST,
  ActionTypes.GET_PATENTS_SUCCESS,
  ActionTypes.GET_PATENTS_FAILURE)
  <void, string, string>();

export const createPatent = createAsyncAction(
  ActionTypes.CREATE_PATENT_REQUEST,
  ActionTypes.CREATE_PATENT_SUCCESS,
  ActionTypes.CREATE_PATENT_FAILURE)
  <IPatent, any, string>();

export const updatePatent = createAsyncAction(
  ActionTypes.UPDATE_PATENT_REQUEST,
  ActionTypes.UPDATE_PATENT_SUCCESS,
  ActionTypes.UPDATE_PATENT_FAILURE)
  <IPatent, any, string>();

export const createNft = createAsyncAction(
  ActionTypes.CREATE_NFT_REQUEST,
  ActionTypes.CREATE_NFT_SUCCESS,
  ActionTypes.CREATE_NFT_FAILURE)
  <{patentId: string}, any, string>();

export const createMarket = createAsyncAction(
  ActionTypes.CREATE_MARKET_REQUEST,
  ActionTypes.CREATE_MARKET_SUCCESS,
  ActionTypes.CREATE_MARKET_FAILURE)
  <{id: string, gradient: number, initialReservePercentage: number, marketCap: number}, void, string>();
