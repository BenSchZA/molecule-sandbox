// import { ContainerState, ContainerActions } from './types';
import { getType } from 'typesafe-actions';
import * as PatentListActions from './actions';

export const initialState = {
};

function patentReducer(state = initialState, action) {
  switch (action.type) {
    case getType(PatentListActions.getPatents.success):
      return {
        ...action.payload,
      };
    default:
      return state;
  }
}

export default patentReducer;
