import { createAsyncAction } from 'typesafe-actions';
import ActionTypes from './constants';

// TODO Get proper interfaces defined for each of these actions and their payloads

export const getBalance = createAsyncAction(
  ActionTypes.GET_WALLET_BALANCE_REQUEST,
  ActionTypes.GET_WALLET_BALANCE_SUCCESS,
  ActionTypes.GET_WALLET_BALANCE_FAILURE)
<undefined, 
{ 
  compounds: [];
  patents: [];
  nftTokens: [];
  daiBalance: number;
  walletAddress: string;
}, 
string>();