// import { ContainerState, ContainerActions } from './types';
import { getType } from 'typesafe-actions';
import * as WalletActions from './actions';

export const initialState = {
  daiBalance: 0,
    walletAddress: '0x',
    compounds: [],
    escrows: [],
    nfts: []
};

function walletReducer(state = initialState, action) {
  switch (action.type) {
    case getType(WalletActions.getBalance.success):
      return {
        ...action.payload,
      };
    default:
      return state;
  }
}

export default walletReducer;
